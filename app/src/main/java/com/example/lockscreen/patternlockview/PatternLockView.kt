package com.example.lockscreen.patternlockview

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.SystemClock
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import com.example.lockscreen.R
import java.lang.ref.WeakReference

/**
 * PatternLockView support two layout mode:
 * PatternLockView 支持两种布局模式：
 *
 *
 * 1. SpacingPadding mode:
 * If lock_spacing is given, PatternLockView use lock_nodeSize, lock_spacing and lock_padding to layout the view.
 * Detail Rules:
 * a. Use exactly lock_nodeSize, spacing and lock_padding to layout. If insufficient space, try b.
 * b. Keep lock_nodeSize, reduce lock_spacing and lock_padding with equal proportion. If insufficient space, try c.
 * c. Keep lock_spacing and lock_padding, reduce lock_nodeSize. If insufficient space, try d.
 * d. Apply Identical-Area mode.
 *
 *
 * 如果设置了lock_spacing时，PatternLockView会使用lock_nodeSize, lock_spacing, lock_padding去布局
 * 具体布局规则如下：
 * a.精确按照lock_nodeSize, lock_spacing, lock_padding去布局进行布局，如果空间不足采用b规则；
 * b.保持lock_nodeSize大小不变，按比例缩小lock_spacing与lock_padding去布局，如果spacing与padding空间小于0，采用c规则；
 * c.保持lock_spacing与lock_padding，缩小lock_nodeSize，如果lock_nodeSize小于0，采用d规则；
 * d.采用Identical-Area mode；
 *
 *
 * 2. Identical-Area mode:
 * If lock_spacing is NOT given, PatternLockView only use lock_nodeSize to layout the view(lock_spacing and lock_padding are ignored).
 * It divides the whole area into n * n identical cells, and layout the node in the center of each cell
 *
 *
 * 如果未设置lock_spacing时，PatternLockView将只使用lock_nodeSize，而无视lock_spacing与lock_padding去布局。
 * 其会将空间等分为n * n个空间，并将节点居中放置
 *
 * @author xyxyLiu
 * @version 1.0
 */
class PatternLockView : ViewGroup {
    // attributes that can be configured with code (non-persistent)
    private var mIsTouchEnabled = true
    private var mFinishTimeout: Long = 1000
    private var mIsFinishInterruptable = true
    private var mIsAutoLink = false
    private val mNodeList: MutableList<NodeView> = ArrayList()
    private var currentNode: NodeView? = null
    private var mPositionX = 0f
    private var mPositionY = 0f
    private var mNodeSrc: Drawable? = null
    private var mNodeHighlightSrc: Drawable? = null
    private var mNodeCorrectSrc: Drawable? = null
    private var mNodeErrorSrc: Drawable? = null
    private var mSize = 0
    private var mTotalSize = 0
    private var mNodeAreaExpand = 0f
    private var mNodeOnAnim = 0
    private var mLineWidth = 0f
    private var mLineColor = 0
    private var mLineCorrectColor = 0
    private var mLineErrorColor = 0
    private var mNodeSize = 0f

    // only used in Identical-Area mode, whether to keep each square
    private val mIsSquareArea = true
    private var mPadding = 0f
    private var mSpacing = 0f
    private var mMeasuredPadding = 0f
    private var mMeasuredSpacing = 0f
    private var mVibrator: Vibrator? = null
    private var mEnableVibrate = false
    private var mVibrateTime = 0
    private var mIsPatternVisible = true
    private var mPaint: Paint? = null
    private var mCallBack: CallBack? = null
    private var mOnNodeTouchListener: OnNodeTouchListener? = null
    private var mShowAnimThread: ShowAnimThread? = null
    private var enableVibrator = false
    private val mFinishAction: Runnable? = Runnable {
        reset()
        setTouchEnabled(true)
    }

    @JvmOverloads
    constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initFromAttributes(context, attrs, defStyleAttr)
    }

    fun setEnableVibrator(enable:Boolean){
        enableVibrator = enable
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        initFromAttributes(context, attrs, defStyleAttr)
    }

    @SuppressLint("ResourceAsColor")
    fun setLineColor(@ColorRes color: Int) {
        mPaint!!.setColor(R.color.line_color1)
        mLineColor = color
    }

    fun setCallBack(callBack: CallBack?) {
        mCallBack = callBack
    }

    fun setDrawable1(@DrawableRes draw: Int) {
        mNodeSrc = context.resources.getDrawable(draw)

    }

    fun setResource(@ColorRes color: Int, @DrawableRes draw1: Int, @DrawableRes draw2: Int) {
        mLineColor =  context.resources.getColor(color)
        mNodeSrc = context.resources.getDrawable(draw1)
        mNodeHighlightSrc = context.resources.getDrawable(draw2)

    }


    fun setDrawable2(@DrawableRes draw: Int) {
        mNodeHighlightSrc = context.resources.getDrawable(draw)

    }

    fun setOnNodeTouchListener(callBack: OnNodeTouchListener?) {
        mOnNodeTouchListener = callBack
    }

    fun setTouchEnabled(isEnabled: Boolean) {
        mIsTouchEnabled = isEnabled
    }

    var isPatternVisible: Boolean
        get() = mIsPatternVisible
        set(isVisible) {
            mIsPatternVisible = isVisible
            invalidate()
        }

    /**
     * time delayed of the lock view resetting after user finish input password
     *
     * @param timeout timeout
     */
    fun setFinishTimeout(timeout: Long) {
        var timeout = timeout
        if (timeout < 0) timeout = 0
        mFinishTimeout = timeout
    }

    /**
     * whether user can start a new password input in the period of FinishTimeout
     *
     * @param isInterruptable if true, the lock view will be reset when user touch a new node.
     * if false, the lock view will be reset only when the finish timeout expires
     * @see .setFinishTimeout
     */
    fun setFinishInterruptable(isInterruptable: Boolean) {
        mIsFinishInterruptable = isInterruptable
    }

    /**
     * whether the nodes in the path of two selected nodes will be automatic linked
     *
     * @param isEnabled enabled
     */
    fun setAutoLinkEnabled(isEnabled: Boolean) {
        mIsAutoLink = isEnabled
    }

    fun setSize(size: Int) {
        mSize = size
        mTotalSize = size * size
        setupNodes(mTotalSize)
    }

    /**
     * reset the view, reset nodes states and clear all lines.
     */
    fun reset() {

        mFinishAction?.let { removeCallbacks(it) }
        mNodeList.clear()
        currentNode = null
        for (n in 0 until childCount) {
            val node = getChildAt(n) as NodeView
            node.setState(STATE_NORMAL)
        }
        mPaint!!.style = Paint.Style.STROKE
        mPaint!!.strokeWidth = mLineWidth
        mPaint!!.color = mLineColor
        mPaint!!.isAntiAlias = true
        invalidate()
    }

    fun fakereset(@DrawableRes src: Int) {
        mFinishAction?.let { removeCallbacks(it) }
        mNodeList.clear()
        currentNode = null
        for (n in 0 until childCount) {
            val node = getChildAt(n) as NodeView
            node.setBackgroundDrawable(resources.getDrawable(src))
            node.setState(STATE_NORMAL)
        }
        mPaint!!.style = Paint.Style.STROKE
        mPaint!!.strokeWidth = mLineWidth
        mPaint!!.color = mLineColor
        mPaint!!.isAntiAlias = true
        invalidate()
    }

    /**
     * show pattern with a giving password
     * @param password password
     */
    fun showPassword(password: List<Int>) {
        ensureValidPassword(password)
        stopPasswordAnim()
        reset()
        for (i in password.indices) {
            val curNode = getChildAt(password[i]) as NodeView
            curNode.setState(STATE_HIGHLIGHT, false)
            addNodeToList(curNode, false)
        }
        invalidate()
    }
    /**
     * show pattern animation n times with a giving password, and listen finish callback
     * @param password password
     * @param repeatTime n, -1 means infinitely
     * @param interval time interval in node highlight
     * @param listenner finish listener
     */
    /**
     * show pattern animation repeatedly with a giving password
     * @param password password
     */
    /**
     * show pattern animation n times with a giving password
     * @param password password
     * @param repeatTime n, -1 means infinitely
     */
    /**
     * show pattern animation n times with a giving password
     * @param password password
     * @param repeatTime n, -1 means infinitely
     * @param interval time interval in node highlight
     */
    @JvmOverloads
    fun showPasswordWithAnim(
        password: List<Int>, repeatTime: Int = -1, interval: Long = DEFAULT_REPLAY_INTERVAL,
        listenner: onAnimFinishListener? = null
    ) {
        ensureValidPassword(password)
        stopPasswordAnim()
        reset()
        setTouchEnabled(false)
        mShowAnimThread = ShowAnimThread(this, password)
        mShowAnimThread!!.setRepeatTime(repeatTime)
            .setOnFinishListenner(listenner)
            .setInterval(interval)
            .start()
    }

    /**
     * check if pattern animation is running
     * @return running
     */
    val isPasswordAnim: Boolean
        get() = if (mShowAnimThread != null) mShowAnimThread!!.isRunning else false

    /**
     * stop pattern animation.
     * call it in [Activity.onDestroy] to avoid memory leaks
     */
    fun stopPasswordAnim() {
        if (mShowAnimThread != null) {
            mShowAnimThread!!.end()
            mShowAnimThread = null
        }
    }

    private fun ensureValidPassword(password: List<Int>?) {
        requireNotNull(password) { "password is null!" }
        for (id in password) {
            requireNotNull(id) { "password has null value!" }
            require(!(id < 0 || id >= mTotalSize)) {
                String.format(
                    "password value is invalid: %d, valid range is "
                            + "[%d, %d]", id, 0, mTotalSize - 1
                )
            }
        }
    }

    private fun initFromAttributes(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
        val a =
            getContext().obtainStyledAttributes(attrs, R.styleable.PatternLockView, defStyleAttr, 0)
        val size = a.getInt(R.styleable.PatternLockView_lock_size, 3)
        mNodeSrc = a.getDrawable(R.styleable.PatternLockView_lock_nodeSrc)
        mNodeHighlightSrc = a.getDrawable(R.styleable.PatternLockView_lock_nodeHighlightSrc)
        mNodeCorrectSrc = a.getDrawable(R.styleable.PatternLockView_lock_nodeCorrectSrc)
        mNodeErrorSrc = a.getDrawable(R.styleable.PatternLockView_lock_nodeErrorSrc)
        mNodeSize = a.getDimension(R.styleable.PatternLockView_lock_nodeSize, 0f)
        mNodeAreaExpand = a.getDimension(R.styleable.PatternLockView_lock_nodeTouchExpand, 0f)
        mNodeOnAnim = a.getResourceId(R.styleable.PatternLockView_lock_nodeOnAnim, 0)
        mLineColor = a.getColor(
            R.styleable.PatternLockView_lock_lineColor,
            Color.argb(0xb2, 0xff, 0xff, 0xff)
        )
        mLineCorrectColor =
            a.getColor(R.styleable.PatternLockView_lock_lineCorrectColor, mLineColor)
        mLineErrorColor = a.getColor(R.styleable.PatternLockView_lock_lineErrorColor, mLineColor)
        mLineWidth = a.getDimension(
            R.styleable.PatternLockView_lock_lineWidth, TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 5f, resources.displayMetrics
            ).toInt().toFloat()
        )
        mPadding = a.getDimension(R.styleable.PatternLockView_lock_padding, 0f)
        mSpacing = a.getDimension(R.styleable.PatternLockView_lock_spacing, -1f)
        mIsAutoLink = a.getBoolean(R.styleable.PatternLockView_lock_autoLink, false)
        mEnableVibrate = a.getBoolean(R.styleable.PatternLockView_lock_enableVibrate, false)
        mVibrateTime = a.getInt(R.styleable.PatternLockView_lock_vibrateTime, 20)
        a.recycle()
        check(mNodeSize > 0) { "nodeSize must be provided and larger than zero!" }
        if (mEnableVibrate) {
            mVibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        }
        mPaint = Paint(Paint.DITHER_FLAG)
        mPaint!!.style = Paint.Style.STROKE
        mPaint!!.strokeWidth = mLineWidth
        mPaint!!.color = mLineColor
        mPaint!!.isAntiAlias = true
        setSize(size)
        setWillNotDraw(false)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var needRemeasure = false
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        var width = MeasureSpec.getSize(widthMeasureSpec)
        var height = MeasureSpec.getSize(heightMeasureSpec)
        val gaps = mSize - 1
        var nodesize = mNodeSize
        mMeasuredPadding = mPadding
        mMeasuredSpacing = mSpacing
        var maxNodeWidth: Float
        var maxNodeHeight: Float
        var maxNodeSize: Float
        if (DEBUG) {
            Log.v(TAG, String.format("onMeasure(), raw width = %d, height = %d)", width, height))
        }

        // Spacing&Padding mode:
        if (mSpacing >= 0) {
            maxNodeWidth = (width - mPadding * 2 - mSpacing * gaps) / mSize
            maxNodeHeight = (height - mPadding * 2 - mSpacing * gaps) / mSize
            maxNodeSize = if (maxNodeWidth < maxNodeHeight) maxNodeWidth else maxNodeHeight
            if (DEBUG) {
                Log.v(
                    TAG, String.format(
                        "maxNodeWidth = %f, maxNodeHeight = %f, maxNodeSize = %f)",
                        maxNodeWidth, maxNodeHeight, maxNodeSize
                    )
                )
            }

            // if maximum available nodesize if smaller than desired nodesize with paddings & spacing unchanged
            if (nodesize > maxNodeSize) {
                val xRemains = (width - mSize * nodesize).toInt()
                val yRemains = (height - mSize * nodesize).toInt()
                val minRemains = if (xRemains < yRemains) xRemains else yRemains
                val paddingsAndSpacings = (mPadding * 2 + mSpacing * gaps).toInt()
                if (DEBUG) {
                    Log.d(
                        TAG, String.format(
                            "xRemains = %d, yRemains = %d, before shrink: mPadding = %f, mSpacing = %f",
                            xRemains, yRemains, mPadding, mSpacing
                        )
                    )
                }
                // keep nodesize & shrink paddings and spacing if there are enough space
                if (minRemains > 0 && paddingsAndSpacings > 0) {
                    val shrinkRatio = minRemains.toFloat() / paddingsAndSpacings
                    mMeasuredPadding *= shrinkRatio
                    mMeasuredSpacing *= shrinkRatio
                    if (DEBUG) {
                        Log.d(
                            TAG, String.format(
                                "shrinkRatio = %f, mMeasuredPadding = %f, mMeasuredSpacing = %f",
                                shrinkRatio, mMeasuredPadding, mMeasuredSpacing
                            )
                        )
                    }
                } else { // otherwise shrink nodesize & keep paddings and spacing
                    nodesize = maxNodeSize
                }
            } else {
                if (!isMeasureModeExactly(widthMode)) {
                    width = (mPadding * 2 + mSpacing * gaps + mSize * nodesize).toInt()
                }
                if (!isMeasureModeExactly(heightMode)) {
                    height = (mPadding * 2 + mSpacing * gaps + mSize * nodesize).toInt()
                }
            }


            // if result nodesize is smaller than zero, remeasure without using spacings.
            if (nodesize <= 0) {
                needRemeasure = true
                // remeasure without using mSpacing
                if (DEBUG) {
                    Log.v(TAG, String.format("remeasure without using mSpacing"))
                }
            }
        }

        // Identical-Area mode:
        // if no spacing is provided, divide the whole area into 9 identical area for each node
        if (needRemeasure || mSpacing < 0) {
            mMeasuredSpacing = -1f
            nodesize = mNodeSize
            maxNodeWidth = (width / mSize).toFloat()
            maxNodeHeight = (height / mSize).toFloat()
            maxNodeSize = if (maxNodeWidth < maxNodeHeight) maxNodeWidth else maxNodeHeight
            if (DEBUG) {
                Log.v(
                    TAG, String.format(
                        "maxNodeWidth = %f, maxNodeHeight = %f, maxNodeSize = %f)",
                        maxNodeWidth, maxNodeHeight, maxNodeSize
                    )
                )
            }

            // if maximum available nodesize if smaller than desired nodesize
            if (nodesize > maxNodeSize) {
                nodesize = maxNodeSize
            }
        }
        if (DEBUG) {
            Log.v(TAG, String.format("measured nodeSize = %f)", nodesize))
        }
        if (width > height && !isMeasureModeExactly(widthMode)) {
            width = height
        } else if (width < height && !isMeasureModeExactly(heightMode)) {
            height = width
        }
        setMeasuredDimension(width, height)
        for (i in 0 until childCount) {
            val v = getChildAt(i)
            val widthSpec = MeasureSpec.makeMeasureSpec(nodesize.toInt(), MeasureSpec.EXACTLY)
            val heightSpec = MeasureSpec.makeMeasureSpec(nodesize.toInt(), MeasureSpec.EXACTLY)
            v.measure(widthSpec, heightSpec)
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        val gaps = mSize - 1
        val height = bottom - top
        val width = right - left
        val nodeSize = getChildAt(0).measuredWidth.toFloat()

        // Identical-Area mode:
        if (mMeasuredSpacing < 0) {
            var areaWidth = (width / mSize).toFloat()
            var areaHeight = (height / mSize).toFloat()
            val areaSize = if (areaWidth < areaHeight) areaWidth else areaHeight
            var widthPadding = 0f
            var heightPadding = 0f
            // whether to keep each cell as square (width = height)
            if (mIsSquareArea) {
                areaWidth = areaSize
                areaHeight = areaSize
                widthPadding = (width - mSize * areaSize) / 2
                heightPadding = (height - mSize * areaSize) / 2
            }
            if (DEBUG) {
                Log.v(
                    TAG, String.format(
                        "nodeSize = %f, areaWidth = %f, areaHeight = %f, widthPadding = %f, heightPadding = %f",
                        nodeSize, areaWidth, areaHeight, widthPadding, heightPadding
                    )
                )
            }
            for (n in 0 until mTotalSize) {
                val node = getChildAt(n) as NodeView
                val row = n / mSize
                val col = n % mSize
                val l =
                    (widthPadding + col * areaWidth + (areaWidth - node.measuredWidth) / 2).toInt()
                val t =
                    (heightPadding + row * areaHeight + (areaHeight - node.measuredHeight) / 2).toInt()
                node.layout(l, t, (l + node.measuredWidth), (t + node.measuredHeight))
            }
        } else { // Spacing&Padding mode:
            val widthPadding = (width - mSize * nodeSize - mMeasuredSpacing * gaps) / 2
            val heightPadding = (height - mSize * nodeSize - mMeasuredSpacing * gaps) / 2
            if (DEBUG) {
                Log.v(
                    TAG, String.format(
                        "nodeSize = %f, widthPadding = %f, heightPadding = %f",
                        nodeSize, widthPadding, heightPadding
                    )
                )
            }
            for (n in 0 until mTotalSize) {
                val node = getChildAt(n) as NodeView
                val row = n / mSize
                val col = n % mSize
                val l = (widthPadding + col * (nodeSize + mMeasuredSpacing)).toInt()
                val t = (heightPadding + row * (nodeSize + mMeasuredSpacing)).toInt()
                val r = (l + nodeSize).toInt()
                val b = (t + nodeSize).toInt()
                node.layout(l, t, r, b)
            }
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!mIsTouchEnabled || !isEnabled) {
            return true
        }
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                if (mIsFinishInterruptable && mFinishAction != null) {
                    removeCallbacks(mFinishAction)
                    mFinishAction.run()
                }
                mPositionX = event.x
                mPositionY = event.y
                val nodeAt = getNodeAt(mPositionX, mPositionY)
                if (currentNode == null) {
                    if (nodeAt != null) {
                        currentNode = nodeAt
                        currentNode!!.setState(STATE_HIGHLIGHT)
                        addNodeToList(currentNode!!, true)
                        tryVibrate()
                        invalidate()
                    }
                } else {
                    if (nodeAt != null && !nodeAt.isHighLighted) {
                        if (mIsAutoLink) {
                            autoLinkNode(currentNode!!, nodeAt)
                        }
                        currentNode = nodeAt
                        currentNode!!.setState(STATE_HIGHLIGHT)
                        addNodeToList(currentNode!!, true)
                        tryVibrate()
                    }
                    invalidate()
                }
            }
            MotionEvent.ACTION_MOVE -> {
                mPositionX = event.x
                mPositionY = event.y
                val nodeAt = getNodeAt(mPositionX, mPositionY)
                if (currentNode == null) {
                    if (nodeAt != null) {
                        currentNode = nodeAt
                        currentNode!!.setState(STATE_HIGHLIGHT)
                        addNodeToList(currentNode!!, true)
                        tryVibrate()
                        invalidate()
                    }
                } else {
                    if (nodeAt != null && !nodeAt.isHighLighted) {
                        if (mIsAutoLink) {
                            autoLinkNode(currentNode!!, nodeAt)
                        }
                        currentNode = nodeAt
                        currentNode!!.setState(STATE_HIGHLIGHT)
                        addNodeToList(currentNode!!, true)
                        tryVibrate()
                    }
                    invalidate()
                }
            }
            MotionEvent.ACTION_UP -> if (mNodeList.size > 0) {
                if (!mIsFinishInterruptable) {
                    setTouchEnabled(false)
                }
                if (mCallBack != null) {
                    val result = mCallBack!!.onFinish(Password.buildPassword(mNodeList))
                    setFinishState(result)
                }
                currentNode = null
                invalidate()
                postDelayed(mFinishAction, mFinishTimeout)
            }
        }
        return true
    }

    @SuppressLint("MissingPermission")
    private fun tryVibrate() {
        if (mEnableVibrate) {
            try {
                if (enableVibrator == true)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        mVibrator!!.vibrate(VibrationEffect.createOneShot(mVibrateTime.toLong(), VibrationEffect.DEFAULT_AMPLITUDE))
                    } else {
                        mVibrator!!.vibrate((mVibrateTime.toLong()))
                    }
               // mVibrator!!.vibrate(mVibrateTime.toLong())
            } catch (e: Throwable) {
                e.printStackTrace()
            }
        }
    }

    private fun setupNodes(totalSize: Int) {
        removeAllViews()
        for (n in 0 until totalSize) {
            val node: NodeView = NodeView(
                context, n
            )
            addView(node)
        }
    }

    private fun setFinishState(result: Int) {
        var nodeState = -1
        var lineColor = mLineColor
        if (result == CODE_PASSWORD_CORRECT) {
            nodeState = STATE_CORRECT
            lineColor = mLineCorrectColor
        } else if (result == CODE_PASSWORD_ERROR) {
            nodeState = STATE_ERROR
            lineColor = mLineErrorColor
        }
        if (nodeState >= 0) {
            for (nodeView in mNodeList) {
                nodeView.setState(nodeState)
            }
        }
        if (lineColor != mLineColor) {
            mPaint!!.color = lineColor
        }
    }

    private fun addNodeToList(nodeView: NodeView, triggerTouch: Boolean) {
        mNodeList.add(nodeView)
        if (triggerTouch && mOnNodeTouchListener != null) {
            mOnNodeTouchListener!!.onNodeTouched(nodeView.nodeId)
        }
    }

    /**
     * auto link the nodes between first and second
     * 检测两个节点间的中间检点是否未连接，否则按顺序连接。
     *
     * @param first
     * @param second
     */
    private fun autoLinkNode(first: NodeView, second: NodeView) {
        val xDiff = second.column - first.column
        val yDiff = second.row - first.row
        if (DEBUG) {
            Log.d(
                TAG,
                String.format(
                    "autoLinkNode(%s, %s), xDiff = %d, yDiff = %d",
                    first,
                    second,
                    xDiff,
                    yDiff
                )
            )
        }
        if (yDiff == 0 && xDiff == 0) {
            return
        } else if (yDiff == 0) {
            val row = first.row
            val step = if (xDiff > 0) 1 else -1
            var column = first.column
            while (step.let { column += it; column } != second.column) {
                tryAppendMidNode(row, column)
            }
        } else if (xDiff == 0) {
            val column = first.column
            val step = if (yDiff > 0) 1 else -1
            var row = first.row
            while (step.let { row += it; row } != second.row) {
                tryAppendMidNode(row, column)
            }
        } else {
            val tan = yDiff / xDiff.toFloat()
            val xstep = if (xDiff > 0) 1 else -1
            if (DEBUG) {
                Log.d(TAG, String.format("tan = %f, xstep = %d", tan, xstep))
            }
            var xDelta = 0
            var yDelta = 0f
            while (xstep.let { xDelta += it; xDelta } != xDiff) {
                yDelta = xDelta * tan
                val yDeltaRounded = Math.round(yDelta)
                if (DEBUG) {
                    Log.d(TAG, String.format("xDelta = %d, yDelta = %f", xDelta, yDelta))
                }
                if (Math.abs(yDelta - yDeltaRounded) < 1e-6) {
                    tryAppendMidNode(first.row + yDeltaRounded, first.column + xDelta)
                }
            }
        }
    }

    private fun tryAppendMidNode(row: Int, column: Int) {
        if (DEBUG) {
            Log.d(TAG, String.format("tryAppendMidNode(row = %d, column = %d)", row, column))
        }
        val mid = getChildAt(row * mSize + column) as NodeView
        if (mNodeList.contains(mid)) return
        mid.setState(STATE_HIGHLIGHT)
        addNodeToList(mid, true)
    }

    override fun onDraw(canvas: Canvas) {
        for (i in 0 until mNodeList.size - 1) {
            val first = mNodeList[i]
            val second = mNodeList[i + 1]
            drawPatternLine(
                canvas,
                first.centerX.toFloat(),
                first.centerY.toFloat(),
                second.centerX.toFloat(),
                second.centerY.toFloat()
            )
        }
        if (currentNode != null) {
            drawPatternLine(
                canvas,
                currentNode!!.centerX.toFloat(),
                currentNode!!.centerY.toFloat(),
                mPositionX,
                mPositionY
            )
        }
    }

    private fun drawPatternLine(
        canvas: Canvas,
        startX: Float,
        startY: Float,
        endX: Float,
        endY: Float
    ) {
        if (mIsPatternVisible) {
            canvas.drawLine(startX, startY, endX, endY, mPaint!!)
        }
    }

    private fun getNodeAt(x: Float, y: Float): NodeView? {
        for (n in 0 until childCount) {
            val node = getChildAt(n) as NodeView
            if (!(x >= node.left - mNodeAreaExpand && x < node.right + mNodeAreaExpand)) {
                continue
            }
            if (!(y >= node.top - mNodeAreaExpand && y < node.bottom + mNodeAreaExpand)) {
                continue
            }
            return node
        }
        return null
    }

    private fun isMeasureModeExactly(measureMode: Int): Boolean {
        return measureMode == MeasureSpec.EXACTLY
    }

    /**
     * Callback to handle pattern input complete event
     * 密码处理返回接口
     */
    interface CallBack {
        /**
         * @param password password
         * @return return value 解锁结果返回值：
         * [.CODE_PASSWORD_CORRECT],
         * [.CODE_PASSWORD_ERROR],
         * @see com.reginald.patternlockview.PatternLockView.Password
         */
        fun onFinish(password: Password?): Int
    }

    /**
     * Callback to handle node touch event
     * 节点点击回调监听器接口
     */
    interface OnNodeTouchListener {
        fun onNodeTouched(NodeId: Int)
    }

    inner class NodeView(context: Context?, val nodeId: Int) : View(context) {
        private var mState = Companion.STATE_NORMAL
        val isHighLighted: Boolean
            get() = mState == Companion.STATE_HIGHLIGHT

        fun setState(state: Int) {
            setState(state, true)
        }

        fun setState(state: Int, anim: Boolean) {
            if (mState == state) {
                return
            }
            when (state) {
                Companion.STATE_NORMAL -> {
                    setBackgroundDrawable(mNodeSrc)
                    clearAnimation()
                }
                Companion.STATE_HIGHLIGHT -> {
                    if (mNodeHighlightSrc != null) {
                        setBackgroundDrawable(mNodeHighlightSrc)
                    }
                    if (anim && mNodeOnAnim != 0) {
                        startAnimation(AnimationUtils.loadAnimation(context, mNodeOnAnim))
                    }
                }
                Companion.STATE_CORRECT -> if (mNodeCorrectSrc != null) {
                    setBackgroundDrawable(mNodeCorrectSrc)
                }
                Companion.STATE_ERROR -> if (mNodeErrorSrc != null) {
                    setBackgroundDrawable(mNodeErrorSrc)
                }
            }
            mState = state
        }

        val centerX: Int
            get() = (left + right) / 2
        val centerY: Int
            get() = (top + bottom) / 2
        val row: Int
            get() = nodeId / mSize
        val column: Int
            get() = nodeId % mSize

        override fun equals(obj: Any?): Boolean {
            return if (obj is NodeView && nodeId == obj.nodeId) {
                true
            } else false
        }

        override fun toString(): String {
            return String.format("NodeView[mId = %d, row = %d, column = %d]", nodeId, row, column)
        }


        init {
            setBackgroundDrawable(mNodeSrc)
        }
    }

    class Password(idList: List<Int>) {
        val list: MutableList<Int>
        val string: String
        protected fun buildPasswordString(nodeIdList: List<Int>): String {
            val passwordBuilder = StringBuilder("[")
            for (i in nodeIdList.indices) {
                val id = nodeIdList[i]
                if (i != 0) {
                    passwordBuilder.append("-")
                }
                passwordBuilder.append(id)
            }
            passwordBuilder.append("]")
            return passwordBuilder.toString()
        }

        override fun equals(obj: Any?): Boolean {
            if (this === obj) {
                return true
            }
            if (obj is Password) {
                return string == obj.string
            }
            return false
        }

        override fun hashCode(): Int {
            return string.hashCode()
        }

        override fun toString(): String {
            return "Password{ $string }"
        }

        companion object {
            fun buildPassword(nodeViewList: List<NodeView>): Password {
                val idList: MutableList<Int> = ArrayList()
                for (nodeView in nodeViewList) {
                    val id = nodeView.nodeId
                    idList.add(id)
                }
                return Password(idList)
            }
        }

        init {
            // build password id list
            list = ArrayList()
            for (id in idList) {
                checkNotNull(id) { "id CAN NOT be null!" }
                list.add(id)
            }

            // build password string
            string = buildPasswordString(idList)
        }
    }

    private class ShowAnimThread(lockView: PatternLockView, password: List<Int>) : Thread() {
        private val mViewRef: WeakReference<PatternLockView>
        private val mPassword: List<Int>
        private var mNodeTimeInterval = 500L
        private var mRepeatTime = 1

        @Volatile
        var isRunning = false
            private set

        @Volatile
        private var mStopping = false
        private var mListener: onAnimFinishListener? = null
        fun setRepeatTime(repeatTime: Int): ShowAnimThread {
            mRepeatTime = repeatTime
            return this
        }

        fun setInterval(timeInterval: Long): ShowAnimThread {
            mNodeTimeInterval = timeInterval
            return this
        }

        fun setOnFinishListenner(listener: onAnimFinishListener?): ShowAnimThread {
            mListener = listener
            return this
        }

        override fun run() {
            isRunning = true
            var repeatTime = mRepeatTime
            while (repeatTime < 0 || repeatTime-- > 0) {
                val view = mViewRef.get() ?: break
                var i = 0
                while (i < mPassword.size && !mStopping) {
                    val isInitalNode = i == 0
                    val curNode = view.getChildAt(mPassword[i]) as NodeView
                    view.post(Runnable {
                        if (mStopping) {
                            return@Runnable
                        }
                        if (isInitalNode) {
                            view.reset()
                        }
                        curNode.setState(Companion.STATE_HIGHLIGHT)
                        view.addNodeToList(curNode, false)
                        view.invalidate()
                    })
                    SystemClock.sleep(mNodeTimeInterval)
                    ++i
                }
                if (mStopping) {
                    break
                }
            }
            val view = mViewRef.get()
            view?.post {
                view.setTouchEnabled(true)
                if (!mStopping) {
                    view.showPassword(mPassword)
                }
                if (mListener != null) {
                    mListener!!.onFinish(mStopping)
                }
            }
            isRunning = false
        }

        fun end() {
            mStopping = true
            val view = mViewRef.get()
            view?.reset()
        }

        init {
            mViewRef = WeakReference(lockView)
            mPassword = password
        }
    }

    interface onAnimFinishListener {
        fun onFinish(isStopped: Boolean)
    }

    companion object {
        const val STATE_NORMAL = 0
        const val STATE_HIGHLIGHT = 1
        const val STATE_CORRECT = 2
        const val STATE_ERROR = 3

        /**
         * password correct
         * 解锁正确
         */
        const val CODE_PASSWORD_CORRECT = 1

        /**
         * password error
         * 解锁错误
         */
        const val CODE_PASSWORD_ERROR = 2
        private const val TAG = "PatternLockView"
        private val DEBUG: Boolean = true
        private const val DEFAULT_REPLAY_INTERVAL = 500L
    }
}