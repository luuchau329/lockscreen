package com.example.lockscreen.lockview

import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.VIBRATOR_SERVICE
import android.graphics.Bitmap
import android.graphics.PixelFormat
import android.graphics.drawable.BitmapDrawable
import android.media.MediaPlayer
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.os.VibratorManager
import android.util.AttributeSet
import android.util.Log
import android.view.*
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.annotation.RawRes
import androidx.core.content.ContentProviderCompat.requireContext
import com.example.lockscreen.MainActivity
import com.example.lockscreen.R
import com.example.lockscreen.base.BaseBindingAdapter
import com.example.lockscreen.databinding.LayoutLockBinding
import com.example.lockscreen.databinding.LayoutLockpinBinding
import com.example.lockscreen.databinding.LayoutPatternlockBinding
import com.example.lockscreen.databinding.TestBinding
import com.example.lockscreen.model.NotifiData
import com.example.lockscreen.patternlockview.PatternLockView
import com.example.lockscreen.patternlockview.PatternLockView.CallBack
import com.example.lockscreen.pinlockview.IndicatorDots
import com.example.lockscreen.pinlockview.PinLockListener
import com.example.lockscreen.pinlockview.PinLockView
import com.example.lockscreen.utils.ultils
import com.example.lockscreen.utils.ultils.setDotEmpty
import com.example.lockscreen.utils.ultils.setDotFill
import com.example.lockscreen.utils.ultils.setDrawable1Pattern
import com.example.lockscreen.utils.ultils.setDrawable2Pattern
import com.example.lockscreen.utils.ultils.setLineColorPattern
import com.romainpiel.shimmer.Shimmer
import me.everything.android.ui.overscroll.IOverScrollDecor
import me.everything.android.ui.overscroll.IOverScrollUpdateListener
import me.everything.android.ui.overscroll.VerticalOverScrollBounceEffectDecorator
import me.everything.android.ui.overscroll.adapters.ScrollViewOverScrollDecorAdapter
import java.util.*


@SuppressLint("ResourceAsColor")
class LockView : RelativeLayout {
    constructor(context: Context?, viewGroup: ViewGroup?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}

    // To keep track of activity's foreground/background status
    var isPaused = false
    lateinit var vibarate: Vibrator
    var enableVibrator = false
    var allowVibrator = false

    @RawRes
    var sound: Int = -1
    var use24Format = false
    var notice = false
    var mainActivity = MainActivity()
    var img: Bitmap? = null
        get() = field
        set(value) {
            field = value
        }

    var windowManager: WindowManager
    lateinit var lockpin_patternlayout: View
    lateinit var lockLayout: View
    var notifiData = arrayListOf<NotifiData>()
    var typeLock: Int = 1
    var type: Int = 1

    fun setType(typeLock: Int, type: Int) {
        this.typeLock = typeLock
        this.type = type
    }

    var sDefaultPassword = PatternLockView.Password(Arrays.asList(0, 1, 2, 3, 4, 5))

    init {
        windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        vibarate = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            val vibratorManager =
                context.getSystemService(Context.VIBRATOR_MANAGER_SERVICE) as VibratorManager
            vibratorManager.defaultVibrator
        } else {
            @Suppress("DEPRECATION")
            context.getSystemService(VIBRATOR_SERVICE) as Vibrator
        }

    }

//    val mPinLockListener: PinLockListener = object : PinLockListener {
//        override
//        fun onComplete(pin: String?) {
//
//            if (sound != -1) {
//                val playMedia = MediaPlayer.create(context, sound)
//                playMedia.start()
//            }
//            Log.d("TAG", "Pin complete: $pin")
//            if (ultils.getPassword(context, 1) == pin)
//                offLockScreen()
//            else {
//                Toast.makeText(context, "Incorrect", Toast.LENGTH_SHORT).show()
//
//            }
//            Log.d("offLockScreen", "onComplete: ")
//        }
//
//        override
//        fun onEmpty() {
//            Log.d("TAG", "Pin empty")
//        }
//
//        override
//        fun onPinChange(pinLength: Int, intermediatePin: String?) {
//            if (enableVibrator == true)
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    vibarate.vibrate(
//                        VibrationEffect.createOneShot(
//                            200,
//                            VibrationEffect.DEFAULT_AMPLITUDE
//                        )
//                    )
//                } else {
//                    vibarate.vibrate(200)
//                }
//
//            Log.d(
//                "TAG",
//                "Pin changed, new length $pinLength with intermediate pin $intermediatePin"
//            )
//        }
//    }

//    fun setEnableVibrate(enable: Boolean) {
//            if (enable == false)
//    }


    private fun initLockPin(): View {
        val inflater = LayoutInflater.from(context)
        var layout = LayoutLockpinBinding.inflate(inflater, null, false)
        layout.imgX.visibility = View.GONE
        // val layout = inflater.inflate(R.layout.test, null, false) as ConstraintLayout
        var mPinLockView = layout.pinLockView as PinLockView?
        layout.type = type

        var mIndicatorDots = layout.indicatorDots as IndicatorDots?
        mIndicatorDots?.setDrawable(null, null)
        mIndicatorDots?.setDrawable(setDotFill(type), setDotEmpty(type))

        mPinLockView?.attachIndicatorDots(mIndicatorDots)
        mPinLockView?.setPinLockListener(
            object : PinLockListener {
                override fun onComplete(pin: String?) {

                    if (pin.equals(ultils.getPassword(context, 1))) {
                        Toast.makeText(context, "Correct", Toast.LENGTH_SHORT).show()

                        offLockScreen()
                    } else {
                        layout.txtErr.visibility = View.VISIBLE

                        mPinLockView.resetPinLockView()

                    }
                    if (sound != -1) {
                        val playMedia = MediaPlayer.create(context, sound)
                        playMedia.start()
                    }
                }

                override fun onEmpty() {
                }

                override fun onPinChange(pinLength: Int, intermediatePin: String?) {
                    layout.txtErr.visibility = View.GONE

                    if (enableVibrator == true) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            vibarate.vibrate(
                                VibrationEffect.createOneShot(
                                    200,
                                    VibrationEffect.DEFAULT_AMPLITUDE
                                )
                            )
                        } else {
                            vibarate.vibrate(200)
                        }
                    }
                }
            }
        )
        mPinLockView?.pinLength = 6
        mIndicatorDots?.indicatorType = (IndicatorDots.IndicatorType.FIXED)

        return layout.root
    }

    private fun initLockPattern(): View {
        val inflater = LayoutInflater.from(context)
        var layout = LayoutPatternlockBinding.inflate(inflater, null, false)
        // val layout = inflater.inflate(R.layout.test, null, false) as ConstraintLayout
        layout.type = type
        layout.txtBack.visibility = View.GONE
//        layout.patternLock.setLineColor(setLineColorPattern(type))
//        layout.patternLock.setDrawable1(setDrawable1Pattern(type))
//        layout.patternLock.setDrawable2(setDrawable2Pattern(type))
        layout.patternLock.setEnableVibrator(enableVibrator)
        layout.patternLock.fakereset(setDrawable1Pattern(type))
        layout.patternLock.setResource(
            setLineColorPattern(type),
            setDrawable1Pattern(type),
            setDrawable2Pattern(type)
        )
        layout.patternLock.setCallBack(object : CallBack {
            override fun onFinish(password: PatternLockView.Password?): Int {
                Log.d("TAG", password!!.string)

                return if (password!!.string == ultils.getPassword(context, 2)) {
                    // password is correct
                    if (sound != -1) {
                        val playMedia = MediaPlayer.create(context, sound)
                        playMedia.start()
                    }

                    offLockScreen()

                    PatternLockView.CODE_PASSWORD_CORRECT
                } else {
                    // password is error
                    PatternLockView.CODE_PASSWORD_ERROR
                }
            }
        })
        return layout.root
    }

    private fun initLockView(): View {
        if (typeLock == 1)
            lockpin_patternlayout = initLockPin()
        else if (typeLock == 2) lockpin_patternlayout = initLockPattern()
        val inflater = LayoutInflater.from(context)
        var layout = LayoutLockBinding.inflate(inflater, null, false)
        if (notifiData.isNullOrEmpty()) Log.d("TAG", "null")
        if (notice == true) {
            val adapter = BaseBindingAdapter<NotifiData>(context, R.layout.layout_item_notification)
            adapter.setData(notifiData)
            layout.lstNotifi.adapter = adapter
        }

        if (use24Format == false)
            layout.txtTime.setFormat24Hour(null)

        if (img != null)
            layout.bg.background = BitmapDrawable(getResources(), img)
        Shimmer().setDuration(700).setDirection(Shimmer.ANIMATION_DIRECTION_LTR)
            .start(layout.stxtSuggest)
        val mVertOverScrollEffect: IOverScrollDecor =
            VerticalOverScrollBounceEffectDecorator(ScrollViewOverScrollDecorAdapter(layout.scrollView))

        mVertOverScrollEffect.setOverScrollUpdateListener(IOverScrollUpdateListener { decor, state, offset ->
            if (offset.toInt() < -200) {
                try {
                    windowManager.removeView(lockLayout)
                    windowManager.addView(lockpin_patternlayout, fgetLayoutParams())
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
        return layout.root
    }


    open fun onLockScreen() {
        if (this::lockLayout.isInitialized) {
            if (lockLayout.parent != null)
                return
        }
        lockLayout = initLockView()
        windowManager.addView(lockLayout, fgetLayoutParams())
        systemUiVisibility = fgetSystemUiVisibility()

    }


    open fun offLockScreen() {
        windowManager.removeView(lockpin_patternlayout)
    }

    fun fgetLayoutParams(): WindowManager.LayoutParams? {
//        return WindowManager.LayoutParams(
//            ViewGroup.LayoutParams.MATCH_PARENT,
//            ViewGroup.LayoutParams.MATCH_PARENT,
//            0,
//            0,
//            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
//            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or
//                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS or
//                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or
//                    WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH or
//                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
//            PixelFormat.TRANSLUCENT
//        )
        return WindowManager.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT,
            0,
            0,
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
            } else {
                WindowManager.LayoutParams.TYPE_PHONE;
            },
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or
                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN or
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS or
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or
                    WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH or
                    WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED or
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,
            PixelFormat.TRANSLUCENT
        )

    }

    fun fgetSystemUiVisibility(): Int {

//        ((KeyguardManager)getSystemService(Activity.KEYGUARD_SERVICE)).newKeyguardLock("IN").disableKeyguard();
        return (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        or View.SYSTEM_UI_FLAG_FULLSCREEN
                //   or View.STATUS_BAR_VISIBLE
                )
    }


}