package com.example.lockscreen.model

import androidx.annotation.DrawableRes

data class ItemStyle(
    val typeLock: Int,
    val type: Int,
    @DrawableRes val img: Int,
    val name: String,
    var check: Boolean = false
)
