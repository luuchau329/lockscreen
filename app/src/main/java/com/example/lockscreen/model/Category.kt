package com.example.lockscreen.model

import com.google.gson.annotations.SerializedName

data class Category(
    val cid: String,
    @SerializedName("category_name")
    val categoryName: String,
    @SerializedName("images")
    val image: String,
    var check: Boolean = false,
    val like: Boolean = false,
    var dow: Boolean = false
)
