package com.example.lockscreen.model

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable


data class NotifiData(val stickerName: String?=null, val title: String?=null, val content:String?=null) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(stickerName)
        parcel.writeString(title)
        parcel.writeString(content)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NotifiData> {
        override fun createFromParcel(parcel: Parcel): NotifiData {
            return NotifiData(parcel)
        }

        override fun newArray(size: Int): Array<NotifiData?> {
            return arrayOfNulls(size)
        }
    }
}
