package com.example.lockscreen.model

import com.google.gson.annotations.SerializedName

data class ListCategory( @SerializedName( "MaterialWallpaper")
                         val materialWallpaper: List<Category>)
