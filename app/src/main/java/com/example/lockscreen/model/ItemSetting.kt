package com.example.lockscreen.model

import androidx.annotation.RawRes

data class ItemSetting(val name:String?="No sound" ,val type:Int,@RawRes val raw : Int?=-1)
