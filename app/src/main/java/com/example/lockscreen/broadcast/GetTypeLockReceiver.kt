package com.example.lockscreen.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class GetTypeLockReceiver : BroadcastReceiver() {
    lateinit var listener: TypeListener

    fun setListenerr(listener: TypeListener) {
        this.listener = listener
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d("TAG", "onReceive:GetTypeLockReceiver ")
        val types = intent?.getIntegerArrayListExtra("type")

        listener.getTypeLock(types!![0], types!![1])

    }


    interface TypeListener {
        fun getTypeLock(typelock: Int, type: Int)
    }
}