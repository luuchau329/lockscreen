package com.example.lockscreen.utils

object TAGNAME {
    const val THRESHOLD_CLICK_TIME = 250
    const val TERM_TAG = "term_tag"
    const val POLICY_TAG = "policy_tag"
    const val  ACTION_GET_TYPE_LOCK = "com.example.lockscreen.GET_TYPE_LOCK"
    const val SAVE_PIN_PASSWORD ="pinPref"
    const val SAVE_PATTERN_PASSWORD ="patternPref"
    const val PREFERENCE="MyPref"
    const val CID="cid"
}