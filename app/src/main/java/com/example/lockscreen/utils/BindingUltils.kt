package com.example.lockscreen.utils

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.lockscreen.R
import com.example.lockscreen.model.LockPin
import com.example.lockscreen.pinlockview.PinLockView


@BindingAdapter("onSingleClick")
fun View.setSingleClick(execution: () -> Unit) {
    setOnClickListener(object : View.OnClickListener {
        var lastClickTime: Long = 0

        override fun onClick(p0: View?) {
            if (System.currentTimeMillis() - lastClickTime < TAGNAME.THRESHOLD_CLICK_TIME) return
            lastClickTime = System.currentTimeMillis()
            execution.invoke()
        }
    })
}

@BindingAdapter("setType")
fun setType(v: PinLockView, type: Int) {
    v.setType(type)
}


@BindingAdapter("showImg")
fun View.showImg(item: LockPin) {
    background = when (item.type) {
        1 -> {
            when (item.number) {
                0 -> resources.getDrawable(R.drawable.n0_pin1)
                1 -> resources.getDrawable(R.drawable.n1_pin1)
                2 -> resources.getDrawable(R.drawable.n2_pin1)
                3 -> resources.getDrawable(R.drawable.n3_pin1)
                4 -> resources.getDrawable(R.drawable.n4_pin1)
                5 -> resources.getDrawable(R.drawable.n5_pin1)
                6 -> resources.getDrawable(R.drawable.n6_pin1)
                7 -> resources.getDrawable(R.drawable.n7_pin1)
                8 -> resources.getDrawable(R.drawable.n8_pin1)
                9 -> resources.getDrawable(R.drawable.n9_pin1)
                else -> return
            }
        }
        else -> return
    }
}

@BindingAdapter("setStyleText1Pin")
fun TextView.setStyleText1Pin(type: Int) {
    when (type) {
        1 -> setTextColor(context.resources.getColor(R.color.neutral9))
        2 -> setTextColor(context.resources.getColor(R.color.neutral9))
        3 -> setTextColor(context.resources.getColor(R.color.neutral9))
        5 -> setTextColor(context.resources.getColor(R.color.neutral9))
        7 -> setTextColor(context.resources.getColor(R.color.neutral9))
        8 -> setTextColor(context.resources.getColor(R.color.neutral9))
        10 -> setTextColor(context.resources.getColor(R.color.customcolor1))
        11 -> {
            textSize = context.resources.getDimension(com.intuit.sdp.R.dimen._38sdp)
            setTextColor(context.resources.getColor(R.color.customcolor1))
            typeface = ResourcesCompat.getFont(context, R.font.utm) as Typeface
        }
        else -> setTextColor(context.resources.getColor(R.color.neutral1))
    }
}

@BindingAdapter("setStyleText2Pin")
fun TextView.setStyleText2Pin(type: Int) {
    when (type) {
        1 -> setTextColor(context.resources.getColor(R.color.neutral9))
        3 -> setTextColor(context.resources.getColor(R.color.neutral9))
        5 -> setTextColor(context.resources.getColor(R.color.neutral9))
        6 -> setTextColor(context.resources.getColor(R.color.neutral9))
        7 -> setTextColor(context.resources.getColor(R.color.neutral9))
        else -> setTextColor(context.resources.getColor(R.color.neutral1))
    }
}

// pattern lock view

@BindingAdapter("setStyleText1Pattern")
fun TextView.setStyleText1Pattern(type: Int) {
    when (type) {
        3 -> setTextColor(context.resources.getColor(R.color.text_pattern3_color))
        5 -> {
            setTextColor(context.resources.getColor(R.color.neutral1))
            typeface = ResourcesCompat.getFont(context, R.font.montserrat_regular) as Typeface
        }
        7 -> {
            typeface = ResourcesCompat.getFont(context, R.font.utm_aquarelle) as Typeface

        }
        else -> setTextColor(context.resources.getColor(R.color.neutral9))
    }
}

@BindingAdapter("setStyleText2Pattern")
fun TextView.setStyleText2Pattern(type: Int) {
    when (type) {
        1 -> setTextColor(context.resources.getColor(R.color.neutral9))
        4 -> setTextColor(context.resources.getColor(R.color.neutral9))
        5 -> setTextColor(context.resources.getColor(R.color.neutral9))
        6 -> setTextColor(context.resources.getColor(R.color.neutral9))
        7 -> setTextColor(context.resources.getColor(R.color.neutral9))
        else -> setTextColor(context.resources.getColor(R.color.neutral1))
    }
}

@BindingAdapter("setBackgroundPin")
fun View.setBackgroundPin(type: Int) {
    when (type) {
        1 -> background = resources.getDrawable(R.drawable.bg_pin1)
        2 -> background = resources.getDrawable(R.drawable.bg_pin2)
        3 -> background = resources.getDrawable(R.drawable.bg_pin3)
        4 -> background = resources.getDrawable(R.drawable.bg_pin4)
        5 -> background = resources.getDrawable(R.drawable.bg_pin5)
        6 -> background = resources.getDrawable(R.drawable.bg_pin6)
        7 -> background = resources.getDrawable(R.drawable.bg_pin7)
        8 -> background = resources.getDrawable(R.drawable.bg_pin8)
        9 -> background = resources.getDrawable(R.drawable.bg_pin9)
        10 -> background = resources.getDrawable(R.drawable.bg_pin10)
        11 -> background = resources.getDrawable(R.drawable.bg_pin11)
    }
}


@BindingAdapter("setBackgroundPattern")
fun View.setBackgroundPattern(type: Int) {
    when (type) {
        1 -> background = resources.getDrawable(R.drawable.bg_pin1)
        2 -> background = resources.getDrawable(R.drawable.bg_pattern2)
        3 -> background = resources.getDrawable(R.drawable.bg_pattern3)
        4 -> background = resources.getDrawable(R.drawable.bg_pattern4)
        5 -> background = resources.getDrawable(R.drawable.bg_pattern5)
        6 -> background = resources.getDrawable(R.drawable.bg_pattern6)
        7 -> background = resources.getDrawable(R.drawable.bg_pattern7)
        8 -> background = resources.getDrawable(R.drawable.bg_pattern8)
        9 -> background = resources.getDrawable(R.drawable.bg_pattern9)
        10 -> background = resources.getDrawable(R.drawable.bg_pattern10)
    }
}

@BindingAdapter("imageUrl")
fun ImageView.loadImage(url: String?) {
    val baseUrl = "http://hdwalls.wallzapps.com/upload/"
    if (!url.isNullOrBlank()) {
        Glide.with(context).load(baseUrl + url)
            .override(500, 600)
            .placeholder(R.drawable.error_image)
            .centerCrop()
            .error(R.drawable.error_image).into(this)
    }
}

@BindingAdapter("imgVisibible")
fun ImageView.setVisible(dow: Boolean) {
    visibility = if (dow == true) View.GONE else View.VISIBLE
}

@BindingAdapter("imgVisibible2")
fun ImageView.setVisible2(check: Boolean) {
    visibility = if (check == true) View.VISIBLE else View.GONE
}




