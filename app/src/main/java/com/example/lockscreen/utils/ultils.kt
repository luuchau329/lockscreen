package com.example.lockscreen.utils

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.example.lockscreen.R
import com.example.lockscreen.model.ItemSetting
import com.example.lockscreen.model.ItemStyle
import com.example.lockscreen.utils.TAGNAME.PREFERENCE
import com.example.lockscreen.utils.TAGNAME.SAVE_PATTERN_PASSWORD
import com.example.lockscreen.utils.TAGNAME.SAVE_PIN_PASSWORD


object ultils {

    @SuppressLint("UseCompatLoadingForDrawables")
    fun showImgLockView(context: Context, type: Int): ArrayList<Drawable> {
        var arrDrawable = arrayListOf<Drawable>()
        when (type) {
            1 -> {
                arrDrawable.add(context.resources.getDrawable(R.drawable.n0_pin1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n1_pin1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n2_pin1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n3_pin1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n4_pin1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n5_pin1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n6_pin1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n7_pin1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n8_pin1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n9_pin1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.back_pin1))

            }
            3 -> {
                arrDrawable.add(context.resources.getDrawable(R.drawable.n0_pin2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n1_pin2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n2_pin2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n3_pin2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n4_pin2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n5_pin2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n6_pin2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n7_pin2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n8_pin2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n9_pin2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.back_pin2))
            }

            2 -> {
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin3n0))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin3n1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin3n2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin3n3))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin3n4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin3n5))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin3n6))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin3n7))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin3n8))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin3n9))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin3back))
            }
            4 -> {
                arrDrawable.add(context.resources.getDrawable(R.drawable.n0_pin4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n1_pin4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n2_pin4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n3_pin4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n4_pin4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n5_pin4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n6_pin4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n7_pin4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n8_pin4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.n9_pin4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.back_pin4))
            }
            5 -> {
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin5n0))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin5n1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin5n2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin5n3))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin5n4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin5n5))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin5n6))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin5n7))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin5n8))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin5n9))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin5back))
            }

            6 -> {
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin6n0))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin6n1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin6n2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin6n3))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin6n4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin6n5))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin6n6))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin6n7))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin6n8))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin6n9))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin6back))
            }

            7 -> {
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin7n0))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin7n1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin7n2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin7n3))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin7n4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin7n5))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin7n6))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin7n7))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin7n8))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin7n9))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin7back))
            }

            8 -> {
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin8n0))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin8n1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin8n2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin8n3))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin8n4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin8n5))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin8n6))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin8n7))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin8n8))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin8n9))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin8back))
            }


            9 -> {
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin9n0))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin9n1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin9n2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin9n3))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin9n4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin9n5))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin9n6))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin9n7))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin9n8))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin9n9))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin9back))
            }

            10 -> {
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin10n0))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin10n1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin10n2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin10n3))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin10n4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin10n5))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin10n6))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin10n7))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin10n8))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin10n9))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin10back))
            }

            11 -> {
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin11n0))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin11n1))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin11n2))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin11n3))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin11n4))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin11n5))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin11n6))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin11n7))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin11n8))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin11n9))
                arrDrawable.add(context.resources.getDrawable(R.drawable.pin11back))
            }


            else -> {

            }
        }
        return arrDrawable
    }


    fun setDotFill(type: Int): Int? {
        return when (type) {
            4 -> R.drawable.pin4dotfill
            5 -> R.drawable.pin5dotfill
            6 -> R.drawable.pin6dotfill
            7 -> R.drawable.pin7dotfill
            8 -> R.drawable.pin8dotfill
            9 -> R.drawable.p9dotfill
            10 -> R.drawable.p9dotfill
            11 -> R.drawable.p11dotfill
            else -> null
        }
    }

    fun setDotEmpty(type: Int): Int? {
        return when (type) {
            2 -> R.drawable.pin2dotempty
            5 -> R.drawable.pin5dotempty
            6 -> R.drawable.pin6dotempty
            7 -> R.drawable.pin6dotempty
            8 -> R.drawable.pin6dotempty
            9 -> R.drawable.p9dotempty
            10 -> R.drawable.p9dotempty
            11 -> R.drawable.p9dotempty
            else -> null
        }
    }


    fun isMyServiceRunning(serviceClass: Class<*>, context: Context): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                Log.i("Service status", "Running")
                return true
            }
        }
        Log.i("Service status", "Not running")
        return false
    }


    fun setDrawable1Pattern(type: Int): Int {
        return when (type) {
            1 -> R.drawable.type1_pattern1
            2 -> R.drawable.type1_pattern2
            3 -> R.drawable.type1_pattern3
            4 -> R.drawable.type1_pattern4
            5 -> R.drawable.typ1_pattern5
            6 -> R.drawable.typ1_pattern5
            7 -> R.drawable.typ1_pattern5
            8 -> R.drawable.type1_pattern1
            9 -> R.drawable.type1_pattern1
            10 -> R.drawable.type1_pattern1

            else -> -1
        }
    }

    fun setDrawable2Pattern(type: Int): Int {
        return when (type) {
            1 -> R.drawable.type2_pattern1
            2 -> R.drawable.type2_pattern2
            3 -> R.drawable.type2_pattern3
            4 -> R.drawable.typ2_pattern4
            5 -> R.drawable.typ2_pattern5
            6 -> R.drawable.type2_pattern6
            7 -> R.drawable.type2_pattern7
            8 -> R.drawable.type1_pattern1
            9 -> R.drawable.type1_pattern1
            10 -> R.drawable.type1_pattern1

            else -> -1
        }
    }

    fun setLineColorPattern(type: Int): Int {
        Log.d("TAG", type.toString())
        return when (type) {
            1 -> R.color.line_color1
            2 -> R.color.line_color2
            3 -> R.color.line_color3
            4 -> R.color.line_color4
            7 -> R.color.line_color7

            else -> R.color.neutral9

        }
    }

    fun setPassword(context: Context, pinPassword: String?, patternPassword: String?) {
        val sharedPreferences: SharedPreferences =
            context.getSharedPreferences(PREFERENCE, MODE_PRIVATE)
        val myEdit = sharedPreferences.edit()
        if (pinPassword != null)
            myEdit.putString(SAVE_PIN_PASSWORD, pinPassword)
        if (patternPassword != null)
            myEdit.putString(SAVE_PATTERN_PASSWORD, patternPassword)
        myEdit.commit()
    }

    fun getPassword(context: Context, typeLock: Int): String {
        val sh: SharedPreferences = context.getSharedPreferences(PREFERENCE, MODE_PRIVATE)
        if (typeLock == 2) return sh.getString(SAVE_PATTERN_PASSWORD, "")!!

        return sh.getString(SAVE_PIN_PASSWORD, "")!!
    }

    fun initStyleData(): ArrayList<ItemStyle> {
        return arrayListOf(
            ItemStyle(1, 1, R.drawable.img_pin1, "pin1"),
            ItemStyle(1, 2, R.drawable.img_pin2, "pin2"),
            ItemStyle(1, 3, R.drawable.img_pin3, "pin3"),
            ItemStyle(1, 4, R.drawable.img_pin4, "pin4"),
            ItemStyle(1, 5, R.drawable.img_pin5, "pint5"),
            ItemStyle(1, 6, R.drawable.img_pin6, "pin6"),
            ItemStyle(1, 7, R.drawable.img_pin7, "pin7"),
            ItemStyle(1, 8, R.drawable.img_pin8, "pin8"),
            ItemStyle(1, 9, R.drawable.img_pin9, "pin9"),
            ItemStyle(1, 10, R.drawable.img_pin10, "pin10"),
            ItemStyle(1, 11, R.drawable.img_pin11, "pin11"),
            ItemStyle(2, 1, R.drawable.img_pattern1, "pattern1"),
            ItemStyle(2, 2, R.drawable.img_pattern2, "pattern2"),
            ItemStyle(2, 3, R.drawable.img_pattern3, "pattern3"),
            ItemStyle(2, 4, R.drawable.img_pattern4, "pattern4"),
            ItemStyle(2, 5, R.drawable.img_pattern5, "pattern5"),
            ItemStyle(2, 6, R.drawable.img_pattern6, "pattern6"),
            ItemStyle(2, 7, R.drawable.img_pattern7, "pattern7"),
            ItemStyle(2, 8, R.drawable.img_pattern8, "pattern8"),
            ItemStyle(2, 9, R.drawable.img_pattern9, "pattern9"),
            ItemStyle(2, 10, R.drawable.img_pattern10, "pattern10")
        )
    }


    fun Context.createDialogPassCode() {
        val factory = LayoutInflater.from(this)
        val dialogView: View = factory.inflate(R.layout.dialog_password, null)
        val dialog = AlertDialog.Builder(this).create()

        dialog.setView(dialogView)

        dialogView.findViewById<TextView>(R.id.txtOK).setSingleClick {
            dialog.dismiss()
        }
        dialog.show()
    }


}