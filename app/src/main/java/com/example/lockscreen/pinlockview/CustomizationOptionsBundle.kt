package com.example.lockscreen.pinlockview
import android.graphics.drawable.Drawable

/**
 * The customization options for the buttons in [PinLockView]
 * passed to the [PinLockAdapter] to decorate the individual views
 *
 * Created by aritraroy on 01/06/16.
 */
class CustomizationOptionsBundle {
    var textColor = 0
    var textSize = 0
    var buttonSize = 0
    var buttonBackgroundDrawable: Drawable? = null
    var deleteButtonDrawable: Drawable? = null
    var deleteButtonSize = 0
    var isShowDeleteButton = false
    var deleteButtonPressesColor = 0
}