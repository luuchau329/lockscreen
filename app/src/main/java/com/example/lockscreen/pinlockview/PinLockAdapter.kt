package com.example.lockscreen.pinlockview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PorterDuff
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.lockscreen.R
import com.example.lockscreen.utils.ultils

/**
 * Created by aritraroy on 31/05/16.
 */
class PinLockAdapter(private val mContext: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder?>() {
    var customizationOptions: CustomizationOptionsBundle? = null
    var onItemClickListener: OnNumberClickListener? = null
    var onDeleteClickListener: OnDeleteClickListener? = null
    var pinLength = 0
    private var mKeyValues: IntArray
    var type :Int = -1

    val arrayDrawable :ArrayList<Drawable> by lazy{ ultils.showImgLockView(mContext,type)}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder: RecyclerView.ViewHolder
        val inflater: LayoutInflater = LayoutInflater.from(parent.getContext())
        viewHolder = if (viewType == VIEW_TYPE_NUMBER) {
            val view: View = inflater.inflate(R.layout.layout_number_item, parent, false)
            NumberViewHolder(view)
        } else {
            val view: View = inflater.inflate(R.layout.layout_delete_item, parent, false)
            DeleteViewHolder(view)
        }
        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.getItemViewType() === VIEW_TYPE_NUMBER) {
            val vh1 = holder as NumberViewHolder
            configureNumberButtonHolder(vh1, position)
        } else if (holder.getItemViewType() === VIEW_TYPE_DELETE) {
            val vh2 = holder as DeleteViewHolder
            configureDeleteButtonHolder(vh2)
        }
    }

    private fun configureNumberButtonHolder(holder: NumberViewHolder?, position: Int) {
        if (holder != null) {
            if (position == 9) {
                holder.mNumberButton.visibility = View.GONE
            } else {
//                holder.mNumberButton.text = mKeyValues[position].toString()
                holder.mNumberButton.visibility = View.VISIBLE
                holder.mNumberButton.tag = mKeyValues[position]
                holder.mNumberButton.setImageDrawable(arrayDrawable.get(mKeyValues[position]))
            }
            if (customizationOptions != null) {
//                holder.mNumberButton.setTextColor(customizationOptions!!.textColor)
                if (customizationOptions!!.buttonBackgroundDrawable != null) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        holder.mNumberButton.setBackgroundDrawable(
                            customizationOptions!!.buttonBackgroundDrawable
                        )
                    } else {
                        holder.mNumberButton.background =
                            customizationOptions!!.buttonBackgroundDrawable
                    }
                }
//                holder.mNumberButton.setTextSize(
//                    TypedValue.COMPLEX_UNIT_PX,
//                    customizationOptions!!.textSize.toFloat()
//                )
                val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                    customizationOptions!!.buttonSize,
                    customizationOptions!!.buttonSize
                )
                holder.mNumberButton.layoutParams = params
            }
        }
    }

    private fun configureDeleteButtonHolder(holder: DeleteViewHolder?) {
        if (holder != null) {
            if (customizationOptions!!.isShowDeleteButton && pinLength > 0) {
                holder.mButtonImage.visibility = View.VISIBLE

                if (customizationOptions?.deleteButtonDrawable != null) {
                    holder.mButtonImage.setImageDrawable(customizationOptions?.deleteButtonDrawable)
                }

                holder.mButtonImage.setColorFilter(
                    customizationOptions!!.textColor,
                    PorterDuff.Mode.SRC_ATOP
                )
                val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                    customizationOptions!!.deleteButtonSize,
                    customizationOptions!!.deleteButtonSize
                )
                holder.mButtonImage.layoutParams = params
            } else {
                holder.mButtonImage.visibility = View.GONE
            }
        }
    }

    override fun getItemCount(): Int {
        return 12
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1) {
            VIEW_TYPE_DELETE
        } else VIEW_TYPE_NUMBER
    }

    var keyValues: IntArray?
        get() = mKeyValues
        set(keyValues) {
            mKeyValues = getAdjustKeyValues(keyValues)
            notifyDataSetChanged()
        }

    private fun getAdjustKeyValues(keyValues: IntArray?): IntArray {
        val adjustedKeyValues = IntArray(keyValues!!.size + 1)
        for (i in keyValues.indices) {
            if (i < 9) {
                adjustedKeyValues[i] = keyValues[i]
            } else {
                adjustedKeyValues[i] = -1
                adjustedKeyValues[i + 1] = keyValues[i]
            }
        }
        return adjustedKeyValues
    }

    inner class NumberViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mNumberButton: ImageButton

        init {
            mNumberButton = itemView.findViewById<View>(R.id.button) as ImageButton

            mNumberButton.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
                    if (onItemClickListener != null) {
                        onItemClickListener!!.onNumberClicked(v.tag as Int)
                    }
                }
            })
        }
    }

    inner class DeleteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mDeleteButton: LinearLayout
        var mButtonImage: ImageView

        init {
            mDeleteButton = itemView.findViewById<View>(R.id.button) as LinearLayout
            mButtonImage = itemView.findViewById<View>(R.id.buttonImage) as ImageButton
            mButtonImage.background = arrayDrawable.last()

            if (customizationOptions!!.isShowDeleteButton && pinLength > 0) {
                mDeleteButton.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View) {
                        if (onDeleteClickListener != null) {
                            onDeleteClickListener!!.onDeleteClicked()
                        }
                    }
                })
                mDeleteButton.setOnLongClickListener(object : View.OnLongClickListener {
                    override fun onLongClick(v: View): Boolean {
                        if (onDeleteClickListener != null) {
                            onDeleteClickListener!!.onDeleteLongClicked()
                        }
                        return true
                    }
                })
//                mDeleteButton.setOnTouchListener(object : View.OnTouchListener {
//                    private var rect: Rect? = null
//                    @SuppressLint("ClickableViewAccessibility")
//                    override fun onTouch(v: View, event: MotionEvent): Boolean {
//                        if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                            mButtonImage.setColorFilter(
//                                customizationOptions
//                                !!.deleteButtonPressesColor
//                            )
//                            rect = Rect(v.left, v.top, v.right, v.bottom)
//                        }
//                        if (event.getAction() == MotionEvent.ACTION_UP) {
//                            mButtonImage.clearColorFilter()
//                        }
//                        if (event.getAction() == MotionEvent.ACTION_MOVE) {
//                            if (!rect!!.contains(
//                                    v.left + event.getX().toInt(),
//                                    v.top + event.getY().toInt()
//                                )
//                            ) {
//                                mButtonImage.clearColorFilter()
//                            }
//                        }
//                        return false
//                    }
//                })
            }
        }
    }

    interface OnNumberClickListener {
        fun onNumberClicked(keyValue: Int)
    }

    interface OnDeleteClickListener {
        fun onDeleteClicked()
        fun onDeleteLongClicked()
    }

    companion object {
        private const val VIEW_TYPE_NUMBER = 0
        private const val VIEW_TYPE_DELETE = 1
    }

    init {
        mKeyValues = getAdjustKeyValues(intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 0))
    }
}