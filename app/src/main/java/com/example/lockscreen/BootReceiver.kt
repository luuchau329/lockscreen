package com.example.lockscreen

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi

class BootReceiver() : BroadcastReceiver() {


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onReceive(p0: Context?, p1: Intent?) {
        val action = p1?.getAction();
        if (action.equals(Intent.ACTION_BOOT_COMPLETED)){
            p0?.startService(Intent(p0,MyService::class.java))
        }
        }

}