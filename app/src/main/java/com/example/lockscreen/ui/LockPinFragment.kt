package com.example.lockscreen.ui

import android.util.Log
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import com.example.lockscreen.MainActivity
import com.example.lockscreen.R
import com.example.lockscreen.base.BaseFragment
import com.example.lockscreen.databinding.LayoutLockpinBinding
import com.example.lockscreen.pinlockview.IndicatorDots
import com.example.lockscreen.pinlockview.PinLockListener
import com.example.lockscreen.pinlockview.PinLockView
import com.example.lockscreen.utils.setSingleClick
import com.example.lockscreen.utils.ultils
import com.example.lockscreen.utils.ultils.createDialogPassCode
import com.example.lockscreen.utils.ultils.getPassword
import com.example.lockscreen.utils.ultils.setPassword

class LockPinFragment : BaseFragment<LayoutLockpinBinding>() {
    lateinit var mPinLockView: PinLockView

    companion object {
        var next = 1
        var password = ""
    }


    val mPinLockListener: PinLockListener = object : PinLockListener {
        override
        fun onComplete(pin: String?) {
            mPinLockView.resetPinLockView()
            Log.d("TAG", next.toString())
            when (next) {
                1 -> {
//                    mPinLockView.resetPinLockView()

                    binding.textView.text = resources.getString(R.string.enter_password)

                    if (pin == getPassword(requireContext(), 1)) {
                        next = 2
                        Log.d("TAG", pin!!)
                        binding.textView.text = resources.getString(R.string.set_password)


                    } else {
                        Toast.makeText(requireContext(), "Incorrect", Toast.LENGTH_SHORT).show()
                        Log.d("TAG", pin!!)

                    }

                    return
                }
                2 -> {
//                    mPinLockView.resetPinLockView()
                    next++
                    password = pin!!
                    binding.textView.text = resources.getString(R.string.confirm_password)
                    return

                }

                3 -> {
                    if (pin != password) {
                        requireContext().createDialogPassCode()
                    } else {
                        setPassword(requireContext(), pin, null)
                        NavHostFragment.findNavController(this@LockPinFragment)
                            .navigate(R.id.action_lockPinFragment_to_homeFragment)
                    }
                }
            }

        }

        override
        fun onEmpty() {
//            Log.d("TAG", "Pin empty")
        }

        override
        fun onPinChange(pinLength: Int, intermediatePin: String?) {
            Log.d(
                "TAG",
                "Pin changed, new length $pinLength with intermediate pin $intermediatePin"
            )
        }
    }

    override fun onClick() {
        binding.imgX.setSingleClick {
            NavHostFragment.findNavController(this@LockPinFragment)
                .navigate(R.id.action_lockPinFragment_to_homeFragment)
        }
    }

    override fun initData() {
        if (getPassword(requireContext(), 1) == "") {
            next = 2
            binding.textView.text = resources.getString(R.string.set_password)

        } else {
            next = 1
            binding.textView.text = resources.getString(R.string.enter_password)

        }
        mPinLockView = (binding.pinLockView as PinLockView?)!!
        var type = MainActivity.type
        binding.type = type

        var mIndicatorDots = binding.indicatorDots as IndicatorDots?
        mIndicatorDots?.setDrawable(ultils.setDotFill(type), ultils.setDotEmpty(type))

        mPinLockView.attachIndicatorDots(mIndicatorDots)
        mPinLockView.setPinLockListener(mPinLockListener)
        mPinLockView.pinLength = (6)
        mIndicatorDots?.indicatorType = (IndicatorDots.IndicatorType.FIXED)

    }

    override fun getLayoutID(): Int {
        return R.layout.layout_lockpin
    }
}