package com.example.lockscreen.ui

import android.util.Log
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import com.example.lockscreen.MainActivity
import com.example.lockscreen.R
import com.example.lockscreen.base.BaseFragment
import com.example.lockscreen.databinding.LayoutPatternlockBinding
import com.example.lockscreen.patternlockview.PatternLockView
import com.example.lockscreen.utils.setSingleClick
import com.example.lockscreen.utils.ultils
import java.util.*

class LockPatternFragment : BaseFragment<LayoutPatternlockBinding>() {
    var next = 1
    var listNumber = arrayListOf<Int>()

    override fun onClick() {
    }

    override fun initData() {
        if (ultils.getPassword(requireContext(), 2) == "") {
            next = 2
            binding.txtPattern.text = resources.getString(R.string.set_new_pattern)

        } else {
            next = 1
            binding.txtPattern.text = resources.getString(R.string.draw_pattern)

        }
        Log.d("TAG", next.toString())
        var type = MainActivity.type
        if (MainActivity.lockType == 2) {
            binding.type = type
            binding.patternLock.setResource(
                ultils.setLineColorPattern(type),
                ultils.setDrawable1Pattern(type),
                ultils.setDrawable2Pattern(type)
            )
            binding.patternLock.fakereset(ultils.setDrawable1Pattern(type))

        }
        binding.txtBack.setSingleClick {
            NavHostFragment.findNavController(this@LockPatternFragment)
                .navigate(R.id.action_lockPatternFragment_to_homeFragment)
        }
        binding.patternLock.setOnNodeTouchListener(object : PatternLockView.OnNodeTouchListener {
            override fun onNodeTouched(NodeId: Int) {
                if (next == 2)
                    listNumber.add(NodeId)
            }
        })
        binding.patternLock.setCallBack(object : PatternLockView.CallBack {
            override fun onFinish(password: PatternLockView.Password?): Int {
                when (next) {
                    1 -> {
                        binding.txtPattern.text = resources.getString(R.string.draw_pattern)
                        Log.d("TAG", ultils.getPassword(requireContext(), 2))
                        if (password!!.string != ultils.getPassword(requireContext(), 2)) {
                            Toast.makeText(requireContext(), "Incorrect", Toast.LENGTH_SHORT).show()
                            next = 1
                        } else {
                            next = 2
                            binding.txtPattern.text = resources.getString(R.string.set_new_pattern)
                            return PatternLockView.CODE_PASSWORD_CORRECT
                        }
                    }
                    2 -> {
                        if (listNumber.size < 6) {
                            listNumber.clear()

                            next = 2
                            Toast.makeText(
                                requireContext(),
                                "length password must >= 6 ",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            next = 3
                            binding.txtPattern.text = resources.getString(R.string.confirm_pattern)
                            return PatternLockView.CODE_PASSWORD_CORRECT
                        }
                    }

                    3 -> {
                        Log.d("TAG", PatternLockView.Password(listNumber).string)
                        if (password!!.string == PatternLockView.Password(listNumber).string) {
                            ultils.setPassword(
                                requireContext(),
                                null,
                                PatternLockView.Password(listNumber).string
                            )

                            NavHostFragment.findNavController(this@LockPatternFragment)
                                .navigate(R.id.action_lockPatternFragment_to_homeFragment)
                            return PatternLockView.CODE_PASSWORD_CORRECT
                        } else {
                            // password is error
                            Toast.makeText(requireContext(), "Incorrect", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
                return PatternLockView.CODE_PASSWORD_ERROR
            }
        })
    }

    override fun getLayoutID(): Int {
        return R.layout.layout_patternlock
    }
}