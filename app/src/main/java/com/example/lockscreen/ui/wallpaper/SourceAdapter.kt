package com.example.lockscreen.ui.wallpaper

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.lockscreen.databinding.LayoutItemWallpaperBinding
import com.example.lockscreen.model.Category
import com.example.lockscreen.utils.setSingleClick

class SourceAdapter(val context: Context) :
    PagingDataAdapter<Category, SourceAdapter.ReviewerViewHolder>(Comparator) {

    var listener: Listener? = null
        set(value) {
            field = value
        }
        get() = field


    override fun onBindViewHolder(holder: ReviewerViewHolder, position: Int) {
        holder.binding.item = getItem(position)

        holder.binding.imgDow.setSingleClick {
            listener?.dowItem(getItem(position)!!)
        }
        holder.binding.imageView2.setSingleClick {
            if (getItem(position)?.dow == true)
                listener?.onItemClick(getItem(position)!!,holder.binding.imageView2.drawable)
        }
    }


    class ReviewerViewHolder(val binding: LayoutItemWallpaperBinding) :
        RecyclerView.ViewHolder(binding.root)

    object Comparator : DiffUtil.ItemCallback<Category>() {
        override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
            // Id is unique.
            return oldItem.image == newItem.image
        }

        override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewerViewHolder {
        return ReviewerViewHolder(
            LayoutItemWallpaperBinding.inflate(
                LayoutInflater.from(
                    context
                ), parent, false
            )
        )
    }

    interface Listener {
        fun onItemClick(item: Category,drawable: Drawable)
        fun dowItem(item: Category)
    }

}