package com.example.lockscreen.ui.dialog

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import com.example.lockscreen.MainActivity
import com.example.lockscreen.databinding.FragmentFavouritedialogBinding
import com.example.lockscreen.db.Image
import com.example.lockscreen.db.MyDB
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class FavouriteDialogFragment : DialogFragment(), FavouriteAdapter.FavouriteListener {
    lateinit var binding: FragmentFavouritedialogBinding
    lateinit var adapter: FavouriteAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            STYLE_NORMAL,
            android.R.style.Theme_Black_NoTitleBar_Fullscreen
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavouritedialogBinding.inflate(inflater, container, false)
        adapter = FavouriteAdapter(requireContext())
        adapter.favouriteListener = this@FavouriteDialogFragment

        val data = MyDB.getInstance(requireContext()).getRoomDao().getAllFavouriteImage(true)
        GlobalScope.launch {
            data.collect {
                it.find { it.name == MainActivity.saveImg }?.check = true
                adapter.setData(it as ArrayList<Image>)
                binding.lstFavourite.adapter = adapter
            }
        }



        return binding.root
    }

    override fun onItemClicked(item: Image, img: Drawable) {
        dismiss()

        SourceDetailDialogFragment.newInstance(img, item.name)
            .show(requireFragmentManager(), "dialog")
    }


}