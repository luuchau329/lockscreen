package com.example.lockscreen.ui.home

interface HomeListener {
    fun onPasswordClick()
    fun onWallpaperClick()
    fun onStyleClick()
}