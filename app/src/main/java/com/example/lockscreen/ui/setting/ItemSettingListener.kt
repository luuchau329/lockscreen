package com.example.lockscreen.ui.setting

import com.example.lockscreen.base.BaseBindingAdapter

interface ItemSettingListener<T> : BaseBindingAdapter.BaseBindingListener {
    fun onItemClick(item:T)

}