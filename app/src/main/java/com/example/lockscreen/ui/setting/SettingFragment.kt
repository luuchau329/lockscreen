package com.example.lockscreen.ui.setting

import android.app.WallpaperManager
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.graphics.drawable.toBitmap
import androidx.core.os.bundleOf
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import com.example.lockscreen.MainActivity
import com.example.lockscreen.R
import com.example.lockscreen.base.BaseBindingAdapter
import com.example.lockscreen.base.BaseFragment
import com.example.lockscreen.databinding.FragmentSettingBinding
import com.example.lockscreen.model.ItemSetting
import com.example.lockscreen.utils.TAGNAME
import com.example.lockscreen.utils.setSingleClick
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileInputStream


class SettingFragment : BaseFragment<FragmentSettingBinding>(), ItemSettingListener<ItemSetting> {
    private var myAdapter: BaseBindingAdapter<ItemSetting>? = null
    val myWallpaperManager: WallpaperManager by lazy {
        WallpaperManager.getInstance(
            requireContext()
        )
    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_setting
    }

    override fun onClick() {
        binding.layoutSoundSetting.root.setSingleClick {
            createDialog(requireContext(), 1, "Sound Setting")

        }

        binding.layoutAddWall.btnSwitch.setOnClickListener {
            if (binding.layoutAddWall.btnSwitch.isChecked == true) {

                GlobalScope.launch(Dispatchers.Default) {
                    if (MainActivity.saveImg != "") {
                        val sdCard = Environment.getExternalStorageDirectory();
                        val directory = File(sdCard.getAbsolutePath() + "/LockScreen");
                        val file =
                            File(directory, MainActivity.saveImg); //or any other format supported
                        val streamIn = FileInputStream(file);
                        ; //This get
                       // var bm = BitmapFactory.decodeStream(streamIn)
                        var bm = requireActivity().resources.getDrawable(R.drawable.bg_pattern10).toBitmap(100,100)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                                Log.d("TAG", bm.toString())
                                myWallpaperManager.setBitmap(
                                    bm,
                                    null,
                                    false,
                                    WallpaperManager.FLAG_LOCK
                                )

                        }



                        streamIn.close()
                    }
                }
            }
        }
        binding.layoutShowBar.btnSwitch.setOnClickListener {
            if (binding.layoutShowBar.btnSwitch.isChecked == true) {
                (requireActivity() as MainActivity).showSystemUi()
                MainActivity.showSystemBar = true
            } else {
                (requireActivity() as MainActivity).hideSystemUi()
                MainActivity.showSystemBar = false

            }
        }

        binding.layoutSleep.root.setSingleClick {
            try {
                if (checkSystemWritePermission())
                    createDialog(requireContext(), 2, "Sleep")
            } catch (e: Exception) {

            }

        }
        binding.layoutPrivacy.root.setSingleClick {
            val bundle = bundleOf("tag" to TAGNAME.POLICY_TAG)
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_settingFragment_to_termFragment, bundle)
        }

        binding.layoutTerm.root.setSingleClick {
            val bundle = bundleOf("tag" to TAGNAME.TERM_TAG)
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_settingFragment_to_termFragment, bundle)

        }
        binding.layoutContact.root.setSingleClick {
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "vuhongvan.fet@gmail.com", null
                )
            )
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Body")
            startActivity(Intent.createChooser(emailIntent, "Send email..."))

        }
        binding.layoutShare.root.setSingleClick {

        }
    }

    override fun initData() {
        myAdapter = BaseBindingAdapter(
            requireContext(), R.layout.item_setting_layout
        )
        myAdapter!!.setListener(this)
    }

    override fun onResume() {
        super.onResume()
        setCheckSetting()
    }

    override fun onItemClick(item: ItemSetting) {
        when (item.type) {
            1 -> {
                if (item.raw != -1) {
                    MainActivity.saveSound = item.raw!!
                    val playMedia = MediaPlayer.create(requireContext(), item.raw)
                    playMedia.start()
                    Toast.makeText(requireContext(), "Select ${item.name}", Toast.LENGTH_SHORT)
                        .show()
                }
            }
            2 -> {
                setTimeout(item.name.toString())
                Toast.makeText(requireContext(), "Select ${item.name}", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun setTimeout(timeout: String) {
        val time: Int
        time = when (timeout) {
            "15s" -> 15000
            "30s" -> 30000
            "1m" -> 60000
            "2m" -> 120000
            "4m" -> 240000
            "10m" -> 600000
            "20m" -> 1200000
            "40m" -> 2400000
            else -> -1
        }
        try {
            Settings.System.putInt(
                requireContext().contentResolver,
                Settings.System.SCREEN_OFF_TIMEOUT, time
            )
            Toast.makeText(requireContext(), "Setting sleep success", Toast.LENGTH_SHORT).show()

        } catch (e: Exception) {
            Toast.makeText(requireContext(), "Setting sleep fail", Toast.LENGTH_SHORT).show()
        }

    }

    fun getList(): List<ItemSetting> = arrayListOf(

        ItemSetting("No sound", 1, -1),
        ItemSetting("Diamond", 1, R.raw.diamond),
        ItemSetting("Dream", 1, R.raw.dream),
        ItemSetting("Drip", 1, R.raw.drip),
        ItemSetting("Fantasy", 1, R.raw.fantasy),
        ItemSetting("Gold", 1, R.raw.gold),
        ItemSetting("Happy", 1, R.raw.happy),
        ItemSetting("Magic", 1, R.raw.magic),
        ItemSetting("Piano", 1, R.raw.piano),
        ItemSetting("Spring", 1, R.raw.spring),
        ItemSetting("Star", 1, R.raw.star),
        ItemSetting("15s", 2),
        ItemSetting("30s", 2),
        ItemSetting("1m", 2),
        ItemSetting("2m", 2),
        ItemSetting("4m", 2),
        ItemSetting("10m", 2),
        ItemSetting("20m", 2),
        ItemSetting("40m", 2)
    )

    private fun checkSystemWritePermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(context)) return true else openAndroidPermissionsMenu()
        }
        return false
    }

    private fun openAndroidPermissionsMenu() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
            intent.data = Uri.parse("package:" + requireContext().packageName)
            requireContext().startActivity(intent)
        }
    }

    fun createDialog(context: Context, type: Int, title: String) {
        val factory = LayoutInflater.from(context)
        val dialogView: View = factory.inflate(R.layout.dialog_setting, null)
        val dialog = AlertDialog.Builder(context).create()

        dialog.setView(dialogView)

//        dialog.window?.setBackgroundDrawableResource(R.drawable.bg1_custom_home)
        myAdapter!!.setData(getList().filter { it.type == type } as ArrayList<ItemSetting>)

        dialogView.findViewById<TextView>(R.id.txtTitle)?.text = title
        dialogView.findViewById<RecyclerView>(R.id.list)?.apply {
            adapter = myAdapter
        }
        dialogView.findViewById<Button>(R.id.imgCancle).setSingleClick {
            dialog.dismiss()
        }
        dialog.show()
    }

    fun setCheckSetting() {
        MainActivity.let {
            binding.layoutAddWall.btnSwitch.isChecked = it.applyWall
            binding.layoutUse24hformat.btnSwitch.isChecked = it.use24hFormat
            binding.layoutShowBar.btnSwitch.isChecked = it.showSystemBar
            binding.layoutPressVibration.btnSwitch.isChecked = it.vibratePress
            binding.layoutAllowVibration.btnSwitch.isChecked = it.allowVibrate
        }
    }

    override fun onStop() {
        super.onStop()

        MainActivity.let {
            it.applyWall = binding.layoutAddWall.btnSwitch.isChecked
            it.use24hFormat = binding.layoutUse24hformat.btnSwitch.isChecked
            it.showSystemBar = binding.layoutShowBar.btnSwitch.isChecked
            it.vibratePress = binding.layoutPressVibration.btnSwitch.isChecked
            it.allowVibrate = binding.layoutAllowVibration.btnSwitch.isChecked
        }
    }
}