package com.example.lockscreen.ui.term

import android.util.Log
import com.example.lockscreen.R
import com.example.lockscreen.base.BaseFragment
import com.example.lockscreen.databinding.FragmentTermBinding
import com.example.lockscreen.utils.TAGNAME
import com.example.lockscreen.utils.setSingleClick

class TermFragment : BaseFragment<FragmentTermBinding>() {
    override fun onClick() {
        binding.imgBack.setSingleClick {
            requireActivity().onBackPressed()
        }
        Log.d("arguments", arguments?.getString("tag").toString())
        binding.isPolicy = arguments?.getString("tag").toString() != TAGNAME.TERM_TAG
    }

    override fun initData() {

    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_term
    }
}