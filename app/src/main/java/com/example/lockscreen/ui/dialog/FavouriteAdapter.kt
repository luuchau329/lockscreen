package com.example.lockscreen.ui.dialog

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.example.lockscreen.R
import com.example.lockscreen.base.BaseBindingAdapter
import com.example.lockscreen.db.Image
import com.example.lockscreen.utils.setSingleClick

class FavouriteAdapter(context: Context?) :
    BaseBindingAdapter<Image>(context, R.layout.layout_item_favourite) {
    var favouriteListener: FavouriteListener? = null
        get() = field
        set(value) {
            field = value
        }
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        super.onBindViewHolder(viewHolder, i)
        viewHolder.itemView.findViewById<ImageView>(R.id.imgFavourtie).let {
            it.setSingleClick {
                favouriteListener!!.onItemClicked(getData()!!.get(i), it.drawable)
            }
        }
    }
    interface FavouriteListener {
        fun onItemClicked(item: Image, img: Drawable)
    }
}