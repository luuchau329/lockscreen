package com.example.lockscreen.ui.wallpaper

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.example.lockscreen.MainActivity
import com.example.lockscreen.R
import com.example.lockscreen.base.BaseFragment
import com.example.lockscreen.databinding.FragmentWallpaperBinding
import com.example.lockscreen.ui.dialog.FavouriteDialogFragment
import com.example.lockscreen.utils.setSingleClick
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.coroutines.*


class WallpaperFragment : BaseFragment<FragmentWallpaperBinding>() {
    private lateinit var viewPager: ViewPager
    lateinit var source: Source


    override fun onClick() {
        binding.imgAddFavourite.setSingleClick {
            FavouriteDialogFragment()
                .show(requireFragmentManager(), "favouritedialog")
        }
    }

    override fun initData() {
        setupViewPager()
    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_wallpaper
    }



    private fun setupViewPager() {
        source = Source()
        (requireActivity() as MainActivity).showLoading()

        GlobalScope.launch(Dispatchers.IO) {
            val cates = source.repository.getListCategory()
            withContext(Dispatchers.Main) {

                viewPager = ViewPager(this@WallpaperFragment, cates!!.map { it -> it.cid })
                binding.viewPager.let {
                    it.adapter = viewPager
                    it.offscreenPageLimit = cates!!.size
                }

                // tab

                binding.tabLayout.addOnTabSelectedListener(object : OnTabSelectedListener {
                    @SuppressLint("UseCompatLoadingForDrawables")
                    override fun onTabSelected(tab: TabLayout.Tab) {
                        customtextTabisSelect(
                            tab,
                            com.intuit.ssp.R.dimen._7ssp,
                            R.color.color_switch,
                            R.font.montserrat_bold,
                            resources.getDrawable(R.drawable.bg_custon_text)
                        )
                    }

                    override fun onTabUnselected(tab: TabLayout.Tab) {
                        customtextTabisSelect(
                            tab,
                            com.intuit.ssp.R.dimen._8ssp,
                            R.color.neutral4,
                            R.font.montserrat_regular,
                            null
                        )

                    }

                    override fun onTabReselected(tab: TabLayout.Tab) {}
                })
                val tabNames = cates?.map { it -> it.categoryName }
                TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
                    tab.text = tabNames?.get(position)

                    tab.setCustomView(
                        createCustomTabView(

                            tabNames?.get(position).toString(),
                        )
                    )
                }.attach()
            }
        }
    }

    private fun createCustomTabView(text: String): View {
        val tabCustomView: View =
            layoutInflater.inflate(R.layout.tab_customview, null)
        val tabTextView: TextView = tabCustomView.findViewById(R.id.tabTV)
        tabTextView.text = text
        tabTextView.textSize = resources.getDimension(com.intuit.ssp.R.dimen._8ssp)
        tabTextView.typeface = ResourcesCompat.getFont(
            tabCustomView.getContext(),
            R.font.montserrat_regular
        ) as Typeface
        tabTextView.setTextColor(
            ContextCompat.getColor(
                tabCustomView.getContext(),
                R.color.neutral4
            )
        )

        return tabCustomView
    }

    private fun customtextTabisSelect(
        tab: TabLayout.Tab,
        textSize: Int,
        textColor: Int,
        textFont: Int,
        backGround: Drawable?
    ) {
        val tabCustomView = tab.customView
        val tabTextView = tabCustomView!!.findViewById<TextView>(R.id.tabTV)
        tabTextView.textSize = resources.getDimension(textSize)
        tabTextView.setTextColor(ContextCompat.getColor(tabCustomView.context, textColor))
        tabTextView.typeface = ResourcesCompat.getFont(tabCustomView.context, textFont) as Typeface
        tabTextView.background = backGround

    }


}