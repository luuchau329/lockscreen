package com.example.lockscreen.ui.style

import com.example.lockscreen.base.BaseBindingAdapter
import com.example.lockscreen.model.ItemStyle

interface ItemStyleListener : BaseBindingAdapter.BaseBindingListener {
    fun onItemClick(item: ItemStyle)
}