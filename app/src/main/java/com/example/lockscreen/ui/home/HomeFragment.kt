package com.example.lockscreen.ui.home

import android.app.admin.DevicePolicyManager
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CompoundButton
import android.widget.Toast
import androidx.core.app.NotificationManagerCompat
import androidx.navigation.fragment.NavHostFragment
import com.example.lockscreen.MainActivity
import com.example.lockscreen.R
import com.example.lockscreen.base.BaseFragment
import com.example.lockscreen.databinding.FragmentHomeBinding
import com.example.lockscreen.utils.setSingleClick
import com.google.firebase.FirebaseApp
import java.io.IOException


class HomeFragment : BaseFragment<FragmentHomeBinding>(), HomeListener {


    override fun getLayoutID(): Int {
        return R.layout.fragment_home
    }

    override fun onPasswordClick() {
        Log.d("TAG", MainActivity.lockType.toString())
        if (MainActivity.lockType == 1)
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_homeFragment_to_lockPinFragment)
        else
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_homeFragment_to_lockPatternFragment)

    }

    override fun onWallpaperClick() {
        if (isConnected()) NavHostFragment.findNavController(this)
            .navigate(R.id.action_homeFragment_to_wallpaperFragment)
        else Toast.makeText(requireContext(), "Require internet connection", Toast.LENGTH_SHORT)
            .show()



    }

    override fun onStyleClick() {
        NavHostFragment.findNavController(this).navigate(R.id.action_homeFragment_to_styleFragment)

//        val crashButton = Button(requireContext())
//        crashButton.text = "Test Crash"
//        crashButton.setOnClickListener {
//            throw RuntimeException("Test Crash") // Force a crash
//        }

//        requireActivity().window.addContentView(crashButton, ViewGroup.LayoutParams(
//            ViewGroup.LayoutParams.MATCH_PARENT,
//            ViewGroup.LayoutParams.WRAP_CONTENT))
    }

    @Throws(InterruptedException::class, IOException::class)
    fun isConnected(): Boolean {
        val command = "ping -c 1 google.com"
        return Runtime.getRuntime().exec(command).waitFor() == 0
    }

    override fun onClick() {


        binding.layoutSetting.root.setOnClickListener {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_homeFragment_to_settingFragment)
        }
        binding.layoutAllowPassword.btnSwitch.setOnCheckedChangeListener(
            object : CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {

                    if (binding.layoutAllowPassword.btnSwitch.isChecked()) {
                        (requireActivity() as MainActivity).addOverlay()
                    } else (requireActivity() as MainActivity).turnOffServie()
                }

            });


        binding.disableLock.root.setSingleClick {
            val intent = Intent(DevicePolicyManager.ACTION_SET_NEW_PASSWORD)
            startActivity(intent)

        }

        binding.layoutNotifi.btnSwitch.setOnCheckedChangeListener(
            object : CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                    if (binding.layoutNotifi.btnSwitch.isChecked()) {
                        if (!NotificationManagerCompat.getEnabledListenerPackages(requireContext())
                                .contains(context?.getPackageName())
                        ) {        //ask for permission
                            val intent =
                                Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS")
                            startActivity(intent)
                        }
//                        val intent =
//                            Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS")
//                        startActivity(intent)
                    }
                }

            });
    }

    override fun initData() {
        binding.listener = this
    }

    override fun onResume() {
        super.onResume()
        setCheck()

    }

    fun setCheck() {
        MainActivity.let {
            binding.layoutAllowPassword.btnSwitch.isChecked = it.allowScreen
            binding.layoutNotifi.btnSwitch.isChecked = it.notice
            binding.layoutRandomWall.btnSwitch.isChecked = it.randomPaper
        }
    }

    override fun onStop() {
        super.onStop()

        MainActivity.let {
            it.allowScreen = binding.layoutAllowPassword.btnSwitch.isChecked
            it.notice = binding.layoutNotifi.btnSwitch.isChecked
            it.randomPaper = binding.layoutRandomWall.btnSwitch.isChecked
        }
    }


}