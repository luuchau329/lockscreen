package com.example.lockscreen.ui.wallpaper

import android.graphics.drawable.Drawable
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import com.bumptech.glide.Glide
import com.example.lockscreen.R
import com.example.lockscreen.base.BaseFragment
import com.example.lockscreen.databinding.FragmentDetailCategoryBinding
import com.example.lockscreen.db.MyDB
import com.example.lockscreen.ui.wallpaper.SourceFragment.Companion.baseUrl
import com.example.lockscreen.utils.setSingleClick

class SourceDetailFragment : BaseFragment<FragmentDetailCategoryBinding>() {
    var name : String = ""
    override fun onClick() {
        binding.apply {
            imgCancle.setSingleClick {
            }

            imgCheck.setSingleClick {
                Toast.makeText(requireContext(), name.toString(), Toast.LENGTH_SHORT).show()
            }

            imgLike.setSingleClick {
                    MyDB.getInstance(requireContext()).getRoomDao().updateLikeImage(true,"")
            }
        }

    }

    override fun initData() {
        name  = arguments?.getString("name","")!!
        Glide.with(requireContext()).load(baseUrl + name)
            .override(500, 600)
            .placeholder(R.drawable.error_image)
            .error(R.drawable.error_image).into(binding.img)
    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_detail_category
    }


}