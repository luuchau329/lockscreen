package com.example.lockscreen.ui.wallpaper

import android.content.Context
import android.util.Log
import androidx.paging.*
import com.example.lockscreen.MainActivity
import com.example.lockscreen.db.MyDB
import com.example.lockscreen.model.Category
import com.example.lockscreen.network.Builder
import com.example.lockscreen.network.Repository
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import java.util.*

class Source {
    val repository: Repository = Repository(Builder.getApiService)
    fun getSearchResultStream(catId: String, context: Context): Flow<PagingData<Category>> {
        return Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { ReviewPagingSource(repository, catId, context) }
        ).flow
    }

//    fun getCategories() : ArrayList<Category> {
//        var data = arrayListOf<Category>()
//        GlobalScope.launch {
//         data.addAll(repository.getListCategory()!!)
//        }
//        return  data
//    }


    inner class ReviewPagingSource(
        val repository: Repository,
        val catId: String,
        val context: Context
    ) : PagingSource<Int, Category>() {

        var response = mutableListOf<Category>()


        override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Category> {
            try {
                // Start refresh at page 1 if undefined.
                val nextPage = params.key ?: 1

                coroutineScope {
                    var imgs =
                        async { MyDB.getInstance(context).getRoomDao().getAllImage() }.await()
                    var data =
                        async { repository.getListbyCategory(catId, nextPage.toString())!! }.await()
                    imgs.forEach {
                        data.find { it1 -> it1.image == it.name }?.dow = true
                        data.find { it1 -> it1.image == MainActivity.saveImg }?.check = true
                    }
                    response = data
                }
//                listImage.collect { listImg ->
//                    Log.d("TAG", listImg.get(0).name.toString())
////                    listImg.forEach { img ->
//////                        Log.d("TAG", response?.find { it.image == img.name }!!.image)
//////                        response?.forEach {
//////                            if (it.image == img.name){
//////                                it.dow = true
//////                            }
//////                        }
////                    }
//                }

                return LoadResult.Page(
                    data = response as List<Category>,
                    prevKey = if (nextPage == 1) null else nextPage - 1,
                    nextKey = if (response.size == 0) null else nextPage + 1
                )
            } catch (e: Exception) {
                return LoadResult.Error(e)
            }
        }

    }
}