package com.example.lockscreen.ui.dialog

import android.annotation.SuppressLint
import android.app.WallpaperManager
import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.DialogFragment
import com.example.lockscreen.MainActivity
import com.example.lockscreen.R
import com.example.lockscreen.databinding.FragmentDetailCategoryBinding
import com.example.lockscreen.db.MyDB
import com.example.lockscreen.ui.wallpaper.SourceFragment
import com.example.lockscreen.utils.setSingleClick

open class SourceDetailDialogFragment : DialogFragment() {
    var checkLiked = false
    lateinit var binding: FragmentDetailCategoryBinding
    val myWallpaperManager: WallpaperManager by lazy {
        WallpaperManager.getInstance(
            requireContext()
        )
    }

    var name = ""
    var img: Drawable? = null
        get() {
            return field
        }
        set(value) {
            field = value
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            STYLE_NORMAL,
            android.R.style.Theme_Black_NoTitleBar_Fullscreen
        );
    }

    companion object {
        fun newInstance(img: Drawable, name: String): SourceDetailDialogFragment {
            val fragment = SourceDetailDialogFragment()
            fragment.name = name
            fragment.img = img
            return fragment
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailCategoryBinding.inflate(inflater, container, false)

        binding.img.setImageDrawable(img)
        val like = MyDB.getInstance(requireContext()).getRoomDao().getLike(name)
        if (like == true) {
            checkLiked = true
            binding.imgLike.setImageDrawable(resources.getDrawable(R.drawable.ic_heart_icon2))
        } else checkLiked = false
        setOnClick()
        return binding.root
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    fun setOnClick() {

        binding.imgCancle.setSingleClick {
            dismiss()
        }

        binding.imgCheck.setSingleClick {
            initOptionDialog(requireContext())
        }

        binding.imgLike.setSingleClick {
            if (checkLiked == true) {
                binding.imgLike.setImageDrawable(resources.getDrawable(R.drawable.ic_heart_icon1))
                checkLiked = false
            } else {
                binding.imgLike.setImageDrawable(requireContext().resources.getDrawable(R.drawable.ic_heart_icon2))
                checkLiked = true
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        MyDB.getInstance(requireContext()).getRoomDao().updateLikeImage(checkLiked, name)

    }


    fun initOptionDialog(context: Context) {
        val factory = LayoutInflater.from(context)
        val dialogView: View = factory.inflate(R.layout.dialog_wall, null)
        val dialog = AlertDialog.Builder(context).create()
        dialog.setView(dialogView)

        dialogView.findViewById<TextView>(R.id.txtSetHome).setSingleClick {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                myWallpaperManager.setBitmap(
                    img!!.toBitmap(),
                    null,
                    false,
                    WallpaperManager.FLAG_SYSTEM
                )
            }
            Toast.makeText(context, "Set wallpaper home screen success", Toast.LENGTH_SHORT).show()
            MainActivity.saveImg = name
            dialog.dismiss()
            requireFragmentManager().fragments.forEach {
                if (it is SourceFragment) it.sourceAdapter.refresh()
            }
        }


        dialogView.findViewById<TextView>(R.id.txtSetLock).setSingleClick {
            Toast.makeText(context, "Set wallpaper lock screen success", Toast.LENGTH_SHORT).show()
            MainActivity.saveImg = name
            dialog.dismiss()
            requireFragmentManager().fragments.forEach {
                if (it is SourceFragment) it.sourceAdapter.refresh()
            }
        }
        dialog.show()
    }
}