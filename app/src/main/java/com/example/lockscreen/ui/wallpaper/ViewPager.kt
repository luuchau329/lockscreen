package com.example.lockscreen.ui.wallpaper

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter

class ViewPager(fragment: Fragment,val listId:List<String>) :
    FragmentStateAdapter(fragment) {

//    var listId = arrayListOf<String>()
//
//    companion object {
//        const val TAB_COUNT = 3
//    }
//
//    fun setUpData(listId: ArrayList<String>) {
//        this.listId = listId
//    }

    private val fragments = mutableListOf<Fragment>()

    override fun getItemCount(): Int {
        return listId.size
    }

    fun getFragment(position: Int): Fragment {
        return fragments.get(position)
    }

    override fun createFragment(position: Int): Fragment {
//        val fragment = when (position) {
//            0 -> SourceFragment.newInstance("")
//            1 -> SourceFragment.newInstance("")
//            2 -> SourceFragment.newInstance("")
//            else -> SourceFragment()
//        }
        val fragment = SourceFragment.newInstance(listId[position])

        fragments.add(position, fragment)
        return fragment
    }
}

