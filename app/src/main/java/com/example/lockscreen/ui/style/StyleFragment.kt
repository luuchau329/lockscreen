package com.example.lockscreen.ui.style

import android.annotation.SuppressLint
import android.util.Log
import androidx.core.content.res.ResourcesCompat
import com.example.lockscreen.MainActivity
import com.example.lockscreen.R
import com.example.lockscreen.base.BaseBindingAdapter
import com.example.lockscreen.base.BaseFragment
import com.example.lockscreen.databinding.LayoutStyleBinding
import com.example.lockscreen.model.ItemStyle
import com.example.lockscreen.utils.setSingleClick
import com.example.lockscreen.utils.ultils.initStyleData


class StyleFragment : BaseFragment<LayoutStyleBinding>(), ItemStyleListener {

    var dataType1 = initStyleData().filter { it -> it.typeLock == 1 } as ArrayList<ItemStyle>
    var dataType2 = initStyleData().filter { it -> it.typeLock == 2 } as ArrayList<ItemStyle>

    var name = ""
    var oldName = ""

    lateinit var adapter: BaseBindingAdapter<ItemStyle>
    override fun getLayoutID(): Int {
        return R.layout.layout_style
    }


    override fun onClick() {

    }

    @SuppressLint("NotifyDataSetChanged")
    fun setCheck() {
        dataType1.forEach {
            Log.d("TAG", MainActivity.lockName)
            if (it.name == MainActivity.lockName) {
                it.check = true
                adapter.notifyDataSetChanged()
            }
        }
        dataType2.forEach {
            if (it.name == MainActivity.lockName) {
                it.check = true
                adapter.notifyDataSetChanged()
            }
        }


    }

    override fun initData() {
        setUpView()
        adapter = BaseBindingAdapter(requireContext(), R.layout.layout_item_style)
        binding.adapter = adapter
        adapter.setData(dataType1)
        adapter.setListener(this)

    }

    override fun onResume() {
        super.onResume()
        setCheck()

    }

    @SuppressLint("ResourceAsColor", "UseCompatLoadingForDrawables")
    fun setUpView() {
        binding.txtPin.let {
            it.setTextColor(resources.getColor(R.color.color_switch))
            it.background = resources.getDrawable(R.drawable.bg_custon_text, null)
            it.typeface = ResourcesCompat.getFont(requireContext(), R.font.montserrat_bold)
        }
        binding.txtPin.setSingleClick {
            adapter.setData(dataType1)
            binding.txtPin.let {
                it.setTextColor(resources.getColor(R.color.color_switch))
                it.background = resources.getDrawable(R.drawable.bg_custon_text, null)
                it.typeface = ResourcesCompat.getFont(requireContext(), R.font.montserrat_bold)
            }
            binding.txtModel.let {
                it.setTextColor(resources.getColor(R.color.neutral4))
                it.background = null
                it.typeface = ResourcesCompat.getFont(requireContext(), R.font.montserrat_regular)
            }
        }
        binding.txtModel.setSingleClick {
            adapter.setData(dataType2)

            binding.txtModel.let {
                it.setTextColor(resources.getColor(R.color.color_switch))
                it.background = resources.getDrawable(R.drawable.bg_custon_text, null)
                it.typeface = ResourcesCompat.getFont(requireContext(), R.font.montserrat_bold)
            }
            binding.txtPin.let {
                it.setTextColor(resources.getColor(R.color.neutral4))
                it.background = null
                it.typeface = ResourcesCompat.getFont(requireContext(), R.font.montserrat_regular)
            }

        }

    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onItemClick(item: ItemStyle) {
        MainActivity.type = item.type
        MainActivity.lockType = item.typeLock
        if (oldName == "")
            oldName = MainActivity.lockName
        else
            oldName = name
        name = item.name
        if (oldName == name) {
            oldName = "null"
        }
        dataType1.forEach {
            if (it.name == name) {
                it.check = true
            }
            if (it.name == oldName) {
                it.check = false
            }

        }
        dataType2.forEach {
            if (it.name == name) {
                it.check = true
            }
            if (it.name == oldName) {
                it.check = false
            }

        }
        adapter.notifyDataSetChanged()

    }

    override fun onStop() {
        super.onStop()
        MainActivity.lockName = name
    }
}