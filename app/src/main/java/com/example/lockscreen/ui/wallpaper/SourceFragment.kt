package com.example.lockscreen.ui.wallpaper

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.paging.LoadState
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.lockscreen.MainActivity
import com.example.lockscreen.R
import com.example.lockscreen.base.BaseFragment
import com.example.lockscreen.databinding.FragmentSourceBinding
import com.example.lockscreen.db.Image
import com.example.lockscreen.db.MyDB
import com.example.lockscreen.model.Category
import com.example.lockscreen.ui.dialog.SourceDetailDialogFragment
import com.example.lockscreen.utils.TAGNAME.CID
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream


class SourceFragment : BaseFragment<FragmentSourceBinding>(), SourceAdapter.Listener {

    var cid: String? = null
    lateinit var source: Source
    lateinit var sourceAdapter: SourceAdapter

    companion object {
        val baseUrl = "http://hdwalls.wallzapps.com/upload/"

        fun newInstance(
            cid: String
        ): SourceFragment {
            val fragment = SourceFragment()
            val bundle = Bundle().apply {
                putString(CID, cid)
            }
            fragment.arguments = bundle
            return fragment
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            cid = it.getString(CID)
        }
    }

    override fun onClick() {
    }


    override fun initData() {
        sourceAdapter = SourceAdapter(requireContext())
        source = Source()
        sourceAdapter.listener = this
        binding.recyclerView.apply {
            setHasFixedSize(true)
            adapter = sourceAdapter

        }
        CoroutineScope(Dispatchers.Main).launch {
            source.getSearchResultStream(cid!!, requireContext()).collect {
                (requireActivity() as MainActivity).progressDialog?.dismiss()

                sourceAdapter.submitData(it)

            }
        }
        sourceAdapter.addLoadStateListener { loadState ->
            /**
            This code is taken from https://medium.com/@yash786agg/jetpack-paging-3-0-android-bae37a56b92d
             **/


            if (loadState.refresh is LoadState.Loading) {
                // show loading
//                (requireActivity() as MainActivity).progressDialog?.show()

            } else {
                // getting the error

//                (requireActivity() as MainActivity).progressDialog?.dismiss()
                val error = when {
                    loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                    loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                    loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                    else -> null
                }
            }

        }

    }

    override fun getLayoutID(): Int {
        return R.layout.fragment_source
    }

    override fun onItemClick(item: Category, drawable: Drawable) {
//        var bundle = Bundle()
//        bundle.putString("name",item.image)
//        NavHostFragment.findNavController(this)
//            .navigate(R.id.action_wallpaperFragment_to_sourceDetailFragment,bundle)
        SourceDetailDialogFragment.newInstance(drawable, item.image)
            .show(requireFragmentManager(), "dialog")

    }

    override fun dowItem(item: Category) {
        downloadImage(baseUrl + item.image)
    }


    fun downloadImage(imageURL: String) {
        if (!verifyPermissions()!!) {
            return
        }
        val dirPath =
            Environment.getExternalStorageDirectory().absolutePath + "/" + getString(R.string.app_name) + "/"

        val dir =if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R)requireContext().applicationContext?.getExternalFilesDir(dirPath)else  File(dirPath)
        val fileName = imageURL.substring(imageURL.lastIndexOf('/') + 1)
        Glide.with(this)
            .load(imageURL)
            .into(object : CustomTarget<Drawable?>() {
                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable?>?
                ) {

                    val bitmap = (resource as BitmapDrawable).bitmap
                    Toast.makeText(requireContext(), "Saving Image...", Toast.LENGTH_SHORT).show()
                    CoroutineScope(Dispatchers.Main).launch {
                        saveImage(bitmap, dir!!, fileName)
                    }
                }

                override fun onLoadFailed(errorDrawable: Drawable?) {
                    Toast.makeText(
                        requireContext(),
                        "Failed to Download Image! Please try again later.",
                        Toast.LENGTH_SHORT
                    ).show();

                }

                override fun onLoadCleared(placeholder: Drawable?) {
                }

            })
    }


    private fun saveImage(image: Bitmap, storageDir: File, imageFileName: String) {
        var successDirCreated = false
        if (!storageDir.exists()) {
            successDirCreated = storageDir.mkdir()
        } else successDirCreated = true

        if (successDirCreated) {
            val imageFile = File(storageDir, imageFileName)
            val savedImagePath: String = imageFile.getAbsolutePath()
            try {
                val fOut: OutputStream = FileOutputStream(imageFile)
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.close()
                Toast.makeText(requireContext(), "Image Saved!", Toast.LENGTH_SHORT).show()
                MyDB.getInstance(requireContext()).getRoomDao()
                    .ínsertImage(Image(imageFileName, false, false))
                sourceAdapter.refresh()
            } catch (e: Exception) {
                Log.d("TAG", e.message.toString())
                Toast.makeText(requireContext(), "Error while saving image!", Toast.LENGTH_SHORT)
                    .show()
                e.printStackTrace()
            }
        } else {
            Toast.makeText(requireContext(), "Failed to make folder!", Toast.LENGTH_SHORT).show()
        }
    }

    fun verifyPermissions(): Boolean? {

        // This will return the current Status
        val permissionExternalMemory =
            ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        if (permissionExternalMemory != PackageManager.PERMISSION_GRANTED) {
            val STORAGE_PERMISSIONS = arrayOf<String>(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            // If permission not granted then ask for permission real time.
            ActivityCompat.requestPermissions(requireActivity(), STORAGE_PERMISSIONS, 1)
            return false
        }
        return true
    }


}