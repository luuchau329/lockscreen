package com.example.lockscreen

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.Environment
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.example.lockscreen.db.MyDB
import com.example.lockscreen.db.Setting
import com.example.lockscreen.lockview.LockView
import com.example.lockscreen.model.NotifiData
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.firstOrNull
import java.io.File
import java.io.FileInputStream
import java.util.*
import kotlin.random.Random


open class MyService : Service() {
    var boot = BootReceiver()
    var db = MyDB.getInstance(this)
    lateinit var bitmap: Bitmap
    lateinit var imgs: List<String>
    val sdCard = Environment.getExternalStorageDirectory();
    val directory = File(sdCard.getAbsolutePath() + "/LockScreen");
    var setting: Setting? = null

    //  private var nReceiver = NotificationReceiver()
//    private var typeLockReceiver = GetTypeLockReceiver()
    var dataNotifiDatas = arrayListOf<NotifiData>()
    val lockView by lazy { LockView(context = this@MyService, viewGroup = null) }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            createNotification()
//        }

        (getSystemService(Activity.KEYGUARD_SERVICE) as KeyguardManager).newKeyguardLock("IN")
            .disableKeyguard()


        GlobalScope.launch {
            val db = MyDB.getInstance(this@MyService).getRoomDao()

            db.getSetting().distinctUntilChanged().firstOrNull().let {
                setting = it
                Log.d("TAG", "2")

            }

            db.getAllImageFlow().collect {
                imgs = it
                Log.d("TAG", "1")
            }
        }


        //        typeLockReceiver.setListenerr(this)
        val filter = IntentFilter().apply {
            addAction(Intent.ACTION_SCREEN_OFF)
            addAction(Intent.ACTION_SCREEN_ON)
            addAction(Intent.ACTION_BOOT_COMPLETED)
            addAction("action.receive.notification")
            addAction("com.example.lockscreen.NOTIFICATION_LISTENER")

        }
//
//        val filterNotifi = IntentFilter()
//        filterNotifi.apply {
//        }

        //  val filtertype = IntentFilter()
//        filtertype.apply {
//            addAction(ACTION_GET_TYPE_LOCK)
//
//        }
        registerReceiver(boot, filter)
//        registerReceiver(typeLockReceiver, filtertype)
//        registerReceiver(nReceiver, filterNotifi)
        showNotifi()

        return START_STICKY
    }

    fun checkSetting() {
        if (MainActivity.appAlive == true) {
            lockView.enableVibrator = MainActivity.vibratePress
            lockView.allowVibrator = MainActivity.allowVibrate
            lockView.use24Format = MainActivity.use24hFormat
            lockView.notice = MainActivity.notice
            lockView.sound = MainActivity.saveSound

            getRandomImg(MainActivity.saveImg, MainActivity.randomPaper)
        } else {
            if (setting != null)
                setting.let {
                    lockView.enableVibrator = it!!.vibratePress!!
                    lockView.allowVibrator = it.allowVibrate!!
                    lockView.use24Format = it.use24hFormat!!
                    lockView.notice = it.notice!!
                    lockView.sound = it.saveSound

                    getRandomImg(it.saveImg, it.randomWallPaper!!)
                }
        }
    }

    fun getRandomImg(saveImg: String, randomImg: Boolean) {
        try {
            if (imgs.size > 0 || !imgs.isNullOrEmpty()) {
                val randomIndex = Random.nextInt(imgs.size);
                val randomElement = imgs[randomIndex]
                Log.d("TAG", "getRandomImg: ${randomElement}")
                var img = if (randomImg == true) randomElement else saveImg
                if (img != "" || !img.isNullOrEmpty()) {
                    val file = File(directory, img); //or any other format supported
                    val streamIn = FileInputStream(file);

                    bitmap = BitmapFactory.decodeStream(streamIn); //This gets the image
                    lockView.img = bitmap
                    streamIn.close();

                }
//        lockView.updateView()
            }
        } catch (e: Exception) {

        }

    }
//    inner class NotificationReceiver : BroadcastReceiver() {
//        override fun onReceive(context: Context, intent: Intent) {
//            Log.d("TAG", intent.action.toString())
//            val datas = intent.getParcelableArrayListExtra<NotifiData>("data")
//            // Log.d("TAG", datas?.get(0)?.stickerName.toString())
//            Log.d("TAG", datas.toString())
//            dataNotifiDatas.clear()
//            dataNotifiDatas.addAll(datas!!)
//            lockView.notifiData = dataNotifiDatas
//
//        }
//    }

    inner class BootReceiver() : BroadcastReceiver() {


        override fun onReceive(p0: Context?, p1: Intent?) {
            val action = p1?.getAction();
            if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                Log.d("BootReceiver", "onReceive1: ")
                startService(Intent(this@MyService, NLService::class.java))

                val getList = Intent("com.example.lockscreen.NOTIFICATION_LISTENER_SERVICE")
                getList.putExtra("notification_event", "getlist")
                sendBroadcast(getList)
                Log.d("TAG", "random viewpaper: " + MainActivity.use24hFormat.toString())
                lockView.setType(MainActivity.lockType, MainActivity.type)
                checkSetting()

            }
            if (action.equals(Intent.ACTION_SCREEN_ON)) {
                Log.d("BootReceiver", "onReceive2: ")

                lockView.onLockScreen()
            }



            if (action.equals("com.example.lockscreen.NOTIFICATION_LISTENER")) {
                val datas = p1?.getParcelableArrayListExtra<NotifiData>("data")
                dataNotifiDatas.clear()
                dataNotifiDatas.addAll(datas!!)
                lockView.notifiData = dataNotifiDatas

            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotification() {


        val NOTIFICATION_CHANNEL_ID = "example.permanence"
        val channelName = "Background Service"
        val chan = NotificationChannel(
            NOTIFICATION_CHANNEL_ID,
            channelName,
            NotificationManager.IMPORTANCE_DEFAULT
        )

        val manager = (getSystemService(NOTIFICATION_SERVICE) as NotificationManager)
        //  val manager = getSystemService(NotificationManager::class.java)
        manager.createNotificationChannel(chan)

        val notificationBuilder = Notification.Builder(this, NOTIFICATION_CHANNEL_ID)
        val notification = notificationBuilder
            .setContentTitle("App is running in background")
            .setContentText("sdgfdghdfhfgh")
            .setPriority(Notification.PRIORITY_DEFAULT)
            .setContentIntent(null)
            .setAutoCancel(false)
            .build()
        startForeground(24234, notification)

    }

    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    fun showNotifi() {
        var notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        // checking if android version is greater than oreo(API 26) or not
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            var notificationChannel =
                NotificationChannel(
                    "channelId",
                    "description",
                    NotificationManager.IMPORTANCE_DEFAULT
                )
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.GREEN
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)

            builder = Notification.Builder(this, "channelId")
                .setContentTitle("App is running in background")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setLargeIcon(
                    BitmapFactory.decodeResource(
                        this.resources,
                        R.drawable.ic_launcher_foreground
                    )
                )
        } else {

            builder = Notification.Builder(this)
                .setContentTitle("App is running in background")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setLargeIcon(
                    BitmapFactory.decodeResource(
                        this.resources,
                        R.drawable.ic_launcher_foreground
                    )
                )
        }
        startForeground(123, builder.build())
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(boot)
    }
}