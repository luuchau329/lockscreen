package com.example.lockscreen.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Image(
    val name: String,
    var check: Boolean = false,
    val like: Boolean,
    @PrimaryKey(autoGenerate = true)
    var id: Int?=null
)
