package com.example.lockscreen.db

import androidx.annotation.RawRes
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Setting(
    @PrimaryKey val id: Int? = 0,
    @ColumnInfo(name = "allow_screen_lock") val allowScreenLock: Boolean? = false,
    @ColumnInfo(name = "notice") val notice: Boolean?,
    @ColumnInfo(name = "random_wall_paper") val randomWallPaper: Boolean? = false,
    @ColumnInfo(name = "apply_wall_paper") val applyWallPaper: Boolean? = false,
    @ColumnInfo(name = "use_24h_format") val use24hFormat: Boolean? = false,
    @ColumnInfo(name = "show_systembar") val showSystembar: Boolean? = false,
    @ColumnInfo(name = "vibrate_press") val vibratePress: Boolean? = false,
    @ColumnInfo(name = "allow_vibrate") val allowVibrate: Boolean? = false,
//    @ColumnInfo(name = "checkLockType") val checkLockType: Boolean?,
    @ColumnInfo(name = "lock_name") val lockName: String? = "",
    @ColumnInfo(name = "lock_type") val lockType: Int = 0,
    @ColumnInfo(name = "type") val typeLock: Int = 0,
    @ColumnInfo(name = "saveImg") val saveImg: String = "",
    @ColumnInfo(name = "saveSound") @RawRes val saveSound: Int = 0,

    )
