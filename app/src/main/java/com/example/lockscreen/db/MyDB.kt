package com.example.lockscreen.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.lockscreen.model.Category

@Database(entities = [Setting::class, Image::class], version = 1)
abstract class MyDB : RoomDatabase() {

    companion object {
        private var INSTANCE: MyDB? = null
        fun getInstance(context: Context): MyDB {

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MyDB::class.java,
                    "db"
                ).allowMainThreadQueries().build()
                INSTANCE = instance
                instance
            }
        }
    }

    abstract fun getRoomDao(): RoomDao

}