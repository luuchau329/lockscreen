package com.example.lockscreen.db

import androidx.annotation.RawRes
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface RoomDao {
    @Query("SELECT * FROM Setting WHERE id = 0")
    fun getSetting(): Flow<Setting>

    @Insert
    fun insertSetting(item: Setting)

    @Query("SELECT saveImg from Setting WHERE id = 0")
    fun getSaveImgName(): String

//    @Query("UPDATE Setting SET allow_screen_lock=:screen, notice=:notice, random_wall_paper=:randomwall WHERE id=0")
//    fun updateHomeSetting(screen: Boolean, notice: Boolean, randomwall: Boolean)

    @Query("UPDATE Setting SET allow_screen_lock=:screen, notice=:notice, random_wall_paper=:randomwall,apply_wall_paper=:applyWall, use_24h_format=:use24h,show_systembar=:showBar,vibrate_press=:press, allow_vibrate=:allowVibrate,lock_type =:lockType,type=:type,lock_name=:lockName,saveImg=:saveImg,saveSound=:saveSound  WHERE id=0")
    fun updateSetting(
        screen: Boolean,
        notice: Boolean,
        randomwall: Boolean,
        applyWall: Boolean,
        use24h: Boolean,
        showBar: Boolean,
        press: Boolean,
        allowVibrate: Boolean,
        lockType: Int,
        type: Int,
        lockName: String,
        saveImg: String,
        @RawRes saveSound :  Int
    )

//    @Query("UPDATE Setting SET lock_name =:name")
//    fun updateLockType(name: String)

    @Query("SELECT * FROM Image ")
    fun getAllImage(): List<Image>

    @Query("SELECT name FROM Image ")
    fun getAllImageFlow(): Flow<List<String>>

    @Query("SELECT `like` FROM Image WHERE name =:name")
    fun getLike(name: String): Boolean

    @Query("SELECT * FROM Image WHERE `like` =:like")
    fun getAllFavouriteImage(like: Boolean): Flow<List<Image>>

    @Insert(onConflict = REPLACE)
    fun ínsertImage(item: Image)

    @Query("UPDATE Image SET `like` =:like WHERE name =:name")
    fun updateLikeImage(like: Boolean, name: String)

//    @Query("UPDATE Image SET `check` =:check WHERE name =:name")
//    fun updateCheckImage(check: Boolean, name: String)


}