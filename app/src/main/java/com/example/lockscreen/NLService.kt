package com.example.lockscreen

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Log
import androidx.annotation.RequiresApi
import com.example.lockscreen.model.NotifiData

class NLService : NotificationListenerService() {
    private val TAG = this.javaClass.simpleName
    private var nlservicereciver: NLServiceReceiver? = null
    override fun onCreate() {
        super.onCreate()

        nlservicereciver = NLServiceReceiver()
        val filter = IntentFilter()
        filter.addAction("com.example.lockscreen.NOTIFICATION_LISTENER_SERVICE")
        registerReceiver(nlservicereciver, filter)
    }

//    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
//        return START_STICKY
//    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(nlservicereciver)
    }

    override fun onNotificationPosted(sbn: StatusBarNotification) {
        Log.i("TAG", "********** onNotificationPosted")


    }

    override fun onNotificationRemoved(sbn: StatusBarNotification) {
        Log.i(TAG, "********** onNOtificationRemoved")
        Log.i(TAG, "ID :" + sbn.id + "t" + sbn.notification.tickerText + "t" + sbn.packageName)
        val i = Intent("com.example.lockscreen.NOTIFICATION_LISTENER_SERVICE")
        i.putExtra("notification_event", "onNotificationRemoved :" + sbn.packageName + "n")
        sendBroadcast(i)
    }

    internal inner class NLServiceReceiver : BroadcastReceiver() {
        @RequiresApi(Build.VERSION_CODES.O)
        override fun onReceive(context: Context, intent: Intent) {
            Log.d("TAG", "onReceive1: ")

            if (intent.getStringExtra("notification_event") == "getlist") {
                var notifis = arrayListOf<NotifiData>()
                for (sbn in this@NLService.activeNotifications) {
                    if (sbn.notification.tickerText != null &&sbn.notification.extras.getString("android.title")!= null && sbn.notification.extras.getString("android.text") != null){
                        notifis.add(NotifiData(sbn.notification.tickerText.toString(),sbn.notification.extras.getString("android.title").toString(),sbn.notification.extras.getString("android.text").toString()))

                    }

                   // i2.putExtra(
                   //     "data",
                     //   NotifiData(
//                            if(sbn.notification.tickerText.toString().isNullOrEmpty())  " " else sbn.notification.tickerText.toString(),
//                            if( sbn.notification.extras.getString("android.title").toString().isNullOrEmpty())  " " else  sbn.notification.extras.getString("android.title").toString(),
//                            if(sbn.notification.extras.getString("android.text").toString().isNullOrEmpty())  " " else sbn.notification.extras.getString("android.text").toString(), "a","b","c"
                       // )
                    //)

                }
                Log.d("TAG", "send list notifi ")
                val i2 = Intent("com.example.lockscreen.NOTIFICATION_LISTENER")
               i2.putParcelableArrayListExtra ("data",notifis)
                sendBroadcast(i2)

            }
        }
    }
}