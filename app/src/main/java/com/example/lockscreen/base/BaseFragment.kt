package com.example.lockscreen.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

abstract class BaseFragment<BD : ViewDataBinding> : Fragment() {
    protected lateinit var binding: BD
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutID(), container, false)
        onClick()
        initData()
        return binding.root
    }
    protected abstract fun onClick()

    protected abstract fun initData()



    protected abstract fun getLayoutID(): Int
}