package com.example.lockscreen.base

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.lockscreen.BR



open class BaseBindingAdapter<T>(context: Context?, @LayoutRes resId: Int) :
    RecyclerView.Adapter<BaseBindingAdapter.ViewHolder>() {
    private var data: ArrayList<T>? = arrayListOf()
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var listener: BaseBindingListener? = null

    @LayoutRes
    private val resId: Int = resId
    fun setData(data: ArrayList<T>?) {
        this.data.apply {
            this?.clear()
            this?.addAll(data!!)
        }

        notifyDataSetChanged()
    }

    fun getData(): List<T>? {
        return data
    }


    fun setListener(listener: BaseBindingListener?) {
        this.listener = listener!!
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                inflater,
                resId,
                viewGroup,
                false
            )
        )
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        val item = data!![i]
        viewHolder.binding.setVariable(BR.item, item)
        if (listener != null) {
            viewHolder.binding.setVariable(BR.listener, listener);
        }

        viewHolder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return if (data == null) 0 else data!!.size
    }

    class ViewHolder(var binding: ViewDataBinding) : RecyclerView.ViewHolder(
        binding.root

    )

    interface BaseBindingListener {

    }
}

