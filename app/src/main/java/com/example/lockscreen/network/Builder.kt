package com.example.lockscreen.network

import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Builder {
    fun retrofit() = Retrofit.Builder()
        .baseUrl("http://hdwalls.wallzapps.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())

        .build()


    val getApiService = retrofit().create(Api::class.java)
}