package com.example.lockscreen.network

import com.example.lockscreen.base.BaseRepository
import com.example.lockscreen.model.Category

class Repository(private val api: Api) : BaseRepository() {

    suspend fun getListCategory(): MutableList<Category>? {
        val data =
            safeApiCall(
                call = { api.getCateGories().await() },
                errorMessage = "Error get image"
            )
        return data?.materialWallpaper?.toMutableList()
    }

    suspend fun getListbyCategory(catId: String, page: String): MutableList<Category>? {

        val query: MutableMap<String, String> = HashMap()
        query["cat_id"] = catId
        query["page"] = page
        val data =
            safeApiCall(
                call = { api.getCateGories(query).await() },
                errorMessage = "Error get image"
            )
        return data?.materialWallpaper?.toMutableList()
    }


}