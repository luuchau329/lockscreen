package com.example.lockscreen.network

import com.example.lockscreen.model.ListCategory
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface Api {

    @GET("api.php")
    fun getCateGories(): Deferred<Response<ListCategory>>

    @GET("api.php")
    fun getCateGories(@QueryMap options: Map<String, String>): Deferred<Response<ListCategory>>

}