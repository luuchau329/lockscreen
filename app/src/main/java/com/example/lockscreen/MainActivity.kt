package com.example.lockscreen

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.View
import android.view.View.SYSTEM_UI_FLAG_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE
import android.view.View.SYSTEM_UI_FLAG_LOW_PROFILE
import android.view.Window
import android.view.WindowInsets
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat.FontCallback.getHandler
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.example.lockscreen.db.MyDB
import com.example.lockscreen.db.Setting
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {

    val TAG = "PinLockView"

    companion object {
        var appAlive = true
        var type = 1
        var lockType = 1
        var allowScreen = false
        var notice = false
        var randomPaper = false
        var applyWall = false
        var use24hFormat = false
        var showSystemBar = false
        var vibratePress = false
        var allowVibrate = false
        var lockName = ""
        var saveImg = ""
        var saveSound = 0
    }

    var progressDialog: ProgressDialog? = null

    fun showLoading() {
        progressDialog = ProgressDialog(this)
        progressDialog?.apply {
            show()
            setContentView(R.layout.layout_loading_custom)
            setCancelable(false)
            window?.setBackgroundDrawableResource(android.R.color.transparent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        if (showSystemBar == false)
            hideSystemUi()
        else showSystemUi()

        setContentView(R.layout.activity_main)
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//        if (showSystemBar == true)
//
//                window.insetsController?.show(WindowInsets.Type.statusBars())
//
//        else window.insetsController?.hide(WindowInsets.Type.statusBars())
//        }
        GlobalScope.launch {
            insertDbOnce()
        }

    }

    fun hideSystemUi() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(true)
        } else {
            // hide status bar
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_IMMERSIVE or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }

    }

    fun showSystemUi() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.setDecorFitsSystemWindows(false)
        } else {
            // Show status bar
            window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            window.decorView.systemUiVisibility = SYSTEM_UI_FLAG_LAYOUT_STABLE
        }

    }

    fun addOverlay() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (!Settings.canDrawOverlays(this)) {
                var intent = Intent(
                    Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName())
                );
                startActivityForResult(intent, 123);
            } else {
                // val serviceIntent = Intent(this,MyService::class.java)
                val serviceIntent = Intent(this, MyService::class.java)
                startService(serviceIntent);
            }
        }
    }

    fun turnOffServie() {
        val serviceIntent = Intent(this, MyService::class.java)
        stopService(serviceIntent);
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 123) {
            if (Settings.canDrawOverlays(this)) {
                // SYSTEM_ALERT_WINDOW permission not granted...
                //Toast.makeText(MyProtector.getContext(), "ACTION_MANAGE_OVERLAY_PERMISSION Permission Granted", Toast.LENGTH_SHORT).show();
                val serviceIntent = Intent(this, MyService::class.java)
                startService(serviceIntent);

            } else {
                Toast.makeText(
                    this,
                    "ACTION_MANAGE_OVERLAY_PERMISSION Permission Denied",
                    Toast.LENGTH_SHORT
                ).show();
            }
        }
    }

    suspend fun insertDbOnce() {
        val db = MyDB.getInstance(this).getRoomDao()
        if (db.getSetting().firstOrNull() == null) {
            MyDB.getInstance(this).getRoomDao().insertSetting(
                Setting(
                    0,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    "",
                    1,
                    1,
                    "",
                    -1
                )
            )
        } else {
            db.getSetting().firstOrNull()?.let {
                allowScreen = it.allowScreenLock!!
                notice = it.notice!!
                randomPaper = it.randomWallPaper!!
                applyWall = it.applyWallPaper!!
                use24hFormat = it.use24hFormat!!
                showSystemBar = it.showSystembar!!
                vibratePress = it.vibratePress!!
                allowVibrate = it.allowVibrate!!
                type = it.typeLock
                lockType = it.lockType
                lockName = it.lockName!!
                saveImg = it.saveImg
                saveSound = it.saveSound

            }
        }

    }

    override fun onStop() {
        super.onStop()

        GlobalScope.launch {
            MyDB.getInstance(this@MainActivity).getRoomDao().updateSetting(
                allowScreen,
                notice,
                randomPaper,
                applyWall,
                use24hFormat,
                showSystemBar,
                vibratePress,
                allowVibrate,
                lockType,
                type,
                lockName,
                saveImg,
                saveSound

            )
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onDestroy() {
        super.onDestroy()
        appAlive = false
        if (Settings.canDrawOverlays(this)) {
            // SYSTEM_ALERT_WINDOW permission not granted...
            //Toast.makeText(MyProtector.getContext(), "ACTION_MANAGE_OVERLAY_PERMISSION Permission Granted", Toast.LENGTH_SHORT).show();
            val serviceIntent = Intent(this, MyService::class.java)
            startService(serviceIntent);

        }
    }

}

